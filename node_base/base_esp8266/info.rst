Basic
=====

Basic node to test scheduling and architecture.

Objective:
 - perform measures at fixed intervals independant of duration of measures.
 - sleep between measures
 - send message once in a while without changing the time between measures

The overall modes of operations are:

  - debug: use serial to send informations
  - config: use wifi to communicate with node and access/set parameters
  - wifi: perform measurements at specified intervals and send them directly through wifi.
  - lora: perform measurements at specified intervals and send them at another time interval using lora network.

Debug mode is not a good idea since it necessitate some usb connection.
So only config/wifi/lora for the moment.
By default the node starts in config mode. On repower it goes again in config mode.
Wifi/lora mode is only accessible through config.
If this setup is not practical, then envisage some pin interrupt to enter
config mode.
Had to use pin D1 to force config on reboot to avoid unplugging USB cable.

Each node has a unique id (settable in config?)
Use MAC address of ESP32 instead?
Also devEui for Lora, same or different?

Ability to check lora config in config?



EPs:

  - RTC memory (cfg)
  - payload
  - html / config
