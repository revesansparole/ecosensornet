#include <ArduinoJson.h>
#include <LittleFS.h>

#include "calibration.h"

Calibration calib;

void calibration::setup() {
  calibration::load_file();
}


void calibration::load_file() {
  StaticJsonDocument<384> doc;
  File fhr = LittleFS.open("calib.json", "r");
  deserializeJson(doc, fhr);
  fhr.close();

#if (DEBUG == 1)
  Serial.println("Loaded calib:");
  String msg = "";
  serializeJsonPretty(doc, msg);
  Serial.println(msg);
#endif

  for (uint8_t i = 0; i < 2; i++) {
    calib.sensor1[i] = doc["sensor1"]["coeffs"][i];
  }
  for (uint8_t i = 0; i < 3; i++) {
    calib.sensor2[i] = doc["sensor2"]["coeffs"][i];
  }

  yield();
}


void calibration::save_file() {
  StaticJsonDocument<384> doc;

  JsonArray sensor1_coeffs = doc["sensor1"].createNestedArray("coeffs");
  for (uint8_t i = 0; i < 2; i++) {
    sensor1_coeffs.add(calib.sensor1[i]);
  }

  JsonArray sensor2_coeffs = doc["sensor2"].createNestedArray("coeffs");
  for (uint8_t i = 0; i < 3; i++) {
    sensor2_coeffs.add(calib.sensor1[i]);
  }

  File fhw = LittleFS.open("calib.json", "w");
  serializeJsonPretty(doc, fhw);
  fhw.close();

#if (DEBUG == 1)
  Serial.println("Saved calib:");
  String msg = "";
  serializeJsonPretty(doc, msg);
  Serial.println(msg);
#endif

  yield();
}
