Communication
=============

Communication can either be done using wifi or lora.

Similar project: https://github.com/timmbogner/Farm-Data-Relay-System

Nodes are responsible for taking measurements at regular intervals and send
information to a gateway.

Modes of operation
------------------

The overall modes of operations are:

  - debug: factory mode to debug non working nodes (potentially use serial to directly communicate and program the node)
  - config: communicate with node to access/set parameters (wifi)
  - local: used to perform measurements in the field while operator is nearby and need to access measures directly.
    Perform measurements at specified intervals and send them directly to a server (through wifi).
  - remote: used to deploy sensors in the field for long periods of time without supervision.
    Perform measurements at specified intervals and send them at another time interval using lora network.

Debug
~~~~~

.. figure:: fig/mode_debug.svg

Config
~~~~~~

.. figure:: fig/mode_config.svg

This mode is used to:

  - fire single measurements to test sensors in the field
  - set wifi settings for local mode (e.g. gateway name)
  - set lora settings for remote mode

Local
~~~~~

.. figure:: fig/mode_local.svg

In this mode data are send directly from the node to the gateway and the computer
is harvesting them at regular intervals.

Remote
~~~~~~

.. figure:: fig/mode_remote.svg

In this mode, the gateway can either directly send data through GSM to remote
influxDB or it can store messages locally to be downloaded later through wifi.

Node API
--------

Node API is a description of all measurements made by a node that are not part
of common data send by a node (e.g. battery voltage).
Node API describes a single measure event.

Message
-------

Node message
~~~~~~~~~~~~

Each node is responsible for sending messages of measurements and ask for actions
to perform.

One measure event captures all sensors connected to the node.
A message contains a list of measure events grouped for saving battery.

Content of measure message:

  - id of node (mac address?)
  - id of node API
  - time [s] between measures as set in config (not real time)
  - number of events in message
  - list of events:

    * battery voltage
    * sensor1
    * sensor2

Date and time are the responsibility of gateway or computer, not node.

Gateway message
~~~~~~~~~~~~~~~

Gateways (or computer in local mode) takes node message and convert it to another
message that include a timestamp.
A getaway message contains a single measure event.
Therefore all events in node messages are unpacked.
Gateway messages ar JSON, therefore no need to keep node API in message.

Content of gateway message:

  - id of node
  - date [UTC] of events (date of node message minus n x time between measures)
  - battery voltage
  - sensor1
  - sensor2


.. note::

    Do I also unpack all sensors to have single measure per message?

      - Yes:

        * single table in database for all measures

      - No:

        * loose coherence between same event through timestamp
        * store messages per node is more relevant

Implementation:

  - messages are transmitted as json strings:

    * both node id and date as fields

  - messages are stored as json files:

    * complete path of message is unique: ./node_id/event_date_iso.json
    * date_iso is not the real iso because of ':' problem, yyyy-mm-ddTHH_MM_SS
