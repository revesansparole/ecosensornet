====================================================
Welcome to ecosensornet's documentation!
====================================================

.. toctree::
    :caption: Contents
    :maxdepth: 2

    introduction
    communication
    config_server
    local_com

.. toctree::
    :caption: Annexe
    :maxdepth: 1

    gitlab home <https://gitlab.com/revesansparole/ecosensornet>
    authors
    badges <badges/listing>
