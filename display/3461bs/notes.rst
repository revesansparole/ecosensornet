74HC595
 - DIO: pin14 DS Serial data input
 - RCLK: pin 12 ST_CP storage register clock pin (latch pin)
 - SCLK: Pin 11 SCH_CP Shift register clock pin


3461BS

D1 D2 D3 D4 (from left to right)

mod0:
 - Q0: 6, D4
 - Q1: 8, D3
 - Q2: 9, D2
 - Q3: 12, D1
 - Q4: na
 - Q5: na
 - Q6: na
 - Q7: na

mod1:
 - Q0: 11, A
 - Q1: 7, B
 - Q2: 4, C
 - Q3: 2, D
 - Q4: 1, E
 - Q5: 10, F
 - Q6: 5, G
 - Q7: 3, decimal

send:
 - first segments (low for segments that need to turn ON)
 - second digit (high for selected digit)
