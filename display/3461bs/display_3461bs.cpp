#include "display_3461bs.h"

uint8_t digits[] = {1, 2, 4, 8};  // address of each digit starting with LSB
uint8_t segs[] = {  // actual binary code to display single digit
  B11000000, // 0
  B11111001, // 1
  B10100100, // 2
  B10110000, // 3
  B10011001, // 4
  B10010010, // 5
  B10000010, // 6
  B11111000, // 7
  B10000000, // 8
  B10011000, // 9
};

Display3461BS::Display3461BS(uint8_t pinLatch, uint8_t pinClock, uint8_t pinData) {
  _pinLatch = pinLatch;
  _pinClock = pinClock;
  _pinData = pinData;
}

void Display3461BS::begin() {
  pinMode(_pinLatch, OUTPUT);
  pinMode(_pinClock, OUTPUT);
  pinMode(_pinData, OUTPUT);
}

void Display3461BS::clear() {
  digitalWrite(_pinLatch, 0);
  shiftOut(255);
  shiftOut(0);
  digitalWrite(_pinLatch, 1);
}

void Display3461BS::display(float value) {
  uint8_t digdis[4];
  uint16_t displayed;
  uint8_t i;

  if (value <= -100) {
    // Serial.println("value too small");
    for (i = 0; i < 4; i++) {
      digdis[i] = B10111111;
    }
  }
  else if (value <= -10) {
    digdis[3] = B10111111;
    displayed = (int)(value * -10 + 0.5);

    for (i = 0; i < 3; i++) {
      digdis[i] = segs[displayed % 10];
      if (i == 1) {
        digdis[i] = digdis[i] & B01111111;
      }
      displayed = displayed / 10;
    }
  }
  else if (value < 0) {
    digdis[3] = B10111111;
    displayed = (int)(value * -100 + 0.5);

    for (i = 0; i < 3; i++) {
      digdis[i] = segs[displayed % 10];
      if (i == 2) {
        digdis[i] = digdis[i] & B01111111;
      }
      displayed = displayed / 10;
    }
  }
  else if (value < 10) {
    displayed = (int)(value * 1000 + 0.5);

    for (i = 0; i < 4; i++) {
      digdis[i] = segs[displayed % 10];
      if (i == 3) {
        digdis[i] = digdis[i] & B01111111;
      }
      displayed = displayed / 10;
    }
  }
  else if (value < 100) {
    displayed = (int)(value * 100 + 0.5);
    
    for (i = 0; i < 4; i++) {
      digdis[i] = segs[displayed % 10];
      if (i == 2) {
        digdis[i] = digdis[i] & B01111111;
      }
      displayed = displayed / 10;
    }
  }
  else {
    // Serial.println("Value too big");
    for (i = 0; i < 4; i++) {
      digdis[i] = B10111110;
    }
  }
  
  for (i = 0; i < 4; i++) {
    //ground latchPin and hold low for as long as you are transmitting
    digitalWrite(_pinLatch, 0);
    shiftOut(digdis[i]);

    // send digit to turn ON
    shiftOut(digits[i]);

    //return the latch pin high to signal chip that it
    //no longer needs to listen for information
    digitalWrite(_pinLatch, 1);
  }
}

void Display3461BS::shiftOut(byte data) {
  // This shifts 8 bits out MSB first,
  //on the rising edge of the clock,
  //clock idles low
  //internal function setup
  int i = 0;
  int pinState;
  //clear everything out just in case to
  //prepare shift register for bit shifting
  digitalWrite(_pinData, 0);
  digitalWrite(_pinClock, 0);
  //for each bit in the byte myDataOut&#xFFFD;
  //NOTICE THAT WE ARE COUNTING DOWN in our for loop
  //This means that %00000001 or "1" will go through such
  //that it will be pin Q0 that lights.
  for (i = 7; i >= 0; i--)  {
    digitalWrite(_pinClock, 0);
    //if the value passed to myDataOut and a bitmask result
    // true then... so if we are at i=6 and our value is
    // %11010100 it would the code compares it to %01000000
    // and proceeds to set pinState to 1.
    if ( data & (1 << i) ) {
      pinState = 1;
    }
    else {
      pinState = 0;
    }
    //Sets the pin to HIGH or LOW depending on pinState
    digitalWrite(_pinData, pinState);
    //register shifts bits on upstroke of clock pin
    digitalWrite(_pinClock, 1);
    //zero the data pin after shift to prevent bleed through
    //digitalWrite(data_pin, 0);
  }
  //stop shifting
  digitalWrite(_pinClock, 0);
}
