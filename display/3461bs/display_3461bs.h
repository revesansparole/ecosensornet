#ifndef DISPLAY_3461BS_H
#define DISPLAY_3461BS_H

#include "Arduino.h"


class Display3461BS {
  public:
    Display3461BS(uint8_t pinLatch, uint8_t pinClock, uint8_t pinData);
    void begin();
    void clear();
    void display(float value);
  private:
    uint8_t _pinLatch;
    uint8_t _pinClock;
    uint8_t _pinData;
    void shiftOut(byte data);
};

#endif  // DISPLAY_3461BS_H
