#include "display_3461bs.h"

Display3461BS disp(12, 13, 14); // (D6, D7, D5);

float temps[] = {-200, -10.12, -1.234, 0., 0.234, 1.234, 10.34, 101};
uint8_t temp_ind = 0;
uint32_t elapsed;

void setup() {
  Serial.begin(115200);

  while (!Serial) {
  }

  delay(1000);

  disp.begin();

  Serial.println("Disp end of setup");
  elapsed = millis();
}

void loop() {
  if (millis() - elapsed > 2000) {
    Serial.println("display");
    disp.clear();
    elapsed = millis();
    temp_ind = (temp_ind + 1) % 8;
  }

  disp.display(temps[temp_ind]);
}
