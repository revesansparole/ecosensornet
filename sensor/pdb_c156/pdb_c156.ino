#include <Wire.h>

#include "component_mcp3424.h"

// 0x68 is the default address for all MCP342x devices
uint8_t address = 0x68;  // 0x6E;
MCP3424 light = MCP3424(address);

void setup(void) {
  Serial.begin(115200);
  Wire.begin();
  delay(300);

  light.begin();
  delay(1); // MC3424 needs 300us to settle, wait 1ms

  // Check device present
  if (!light.is_connected()) {
    Serial.print("No device found at address ");
    Serial.println(light.address(), HEX);
    while (1)
      ;
  }

}

void loop(void) {
  uint8_t err;
  long value = 0;
  MCP3424::Config status;

  err = light.convert_and_read(MCP3424::chn_1, MCP3424::one_shot, MCP3424::res_18, MCP3424::gain_8, 1000000, value, status);
  if (err) {
    Serial.println("Convert error: " + err);
  }
  else {
    Serial.println("light: " + String(value));
  }

  delay(5000);
}
