#include "sensor_ds18b20.h"

DS18B20 ds(5);

void setup() {
  //Vext ON
//  pinMode(Vext, OUTPUT);
//  digitalWrite(Vext, LOW);
  
  Serial.begin(115200);

  while (!Serial) {
  }

  delay(1000);

  Serial.println("Devices: " + String(ds.search()) + " [#]");
}

void loop() {
  Serial.println("try reading");
  while (ds.selectNext()) {
    uint8_t address[8];
    ds.getAddress(address);

    Serial.print("Address:");
    for (uint8_t i = 0; i < 8; i++) {
      Serial.print(" ");
      Serial.print(address[i]);
    }
    Serial.println();

    Serial.println("Temperature: " + String(ds.getTempC(), 2) + " [°C]");
  }

  delay(2000);
}
