#include "sensor_mlx90614.h"

MLX90614::MLX90614() {
  // Set initial values for all private member variables
  _deviceAddress = 0;
  _defaultUnit = TEMP_C;
  _rawObject = 0;
  _rawAmbient = 0;
  _rawObject2 = 0;
  _rawMax = 0;
  _rawMin = 0;
}

bool MLX90614::begin(uint8_t address, TwoWire &wirePort) {
  _deviceAddress = address;
  _wire = &wirePort;

  return true;
}

// I2C address of device
uint8_t MLX90614::address() {
  return _deviceAddress;
}

bool MLX90614::isConnected() {
  _wire->beginTransmission(_deviceAddress);
  if (_wire->endTransmission() == 0) {
    return true;
  }
  return false;
}

void MLX90614::setUnit(temperature_units unit) {
  _defaultUnit = unit;
}

bool MLX90614::read() {
  // read both the object and ambient temperature values
  if (readObject() && readAmbient()) {
    return true;
  }
  return false;
}

bool MLX90614::readRange() {
  // Read both minimum and maximum values from EEPROM
  if (readMin() && readMax())	{
    return true;
  }
  return false;
}

float MLX90614::ambient(void) {
  return calcTemperature(_rawAmbient);
}

float MLX90614::object(void) {
  return calcTemperature(_rawObject);
}

float MLX90614::minimum(void) {
  return calcTemperature(_rawMin);
}

float MLX90614::maximum(void) {
  return calcTemperature(_rawMax);
}

bool MLX90614::readObject() {
  int16_t rawObj;
  // Read from the TOBJ1 register, store into the rawObj variable
  if (I2CReadWord(MLX90614_REGISTER_TOBJ1, &rawObj)) {
    if (rawObj & 0x8000) {// If there was a flag error
      return false; // Return fail
    }
    // Store the object temperature into the class variable
    _rawObject = rawObj;
    return true;
  }
  return false;
}

bool MLX90614::readObject2() {
  int16_t rawObj;
  // Read from the TOBJ2 register, store into the rawObj variable
  if (I2CReadWord(MLX90614_REGISTER_TOBJ2, &rawObj)) {
    // If the read succeeded
    if (rawObj & 0x8000) {
      return false;
    }
    _rawObject2 = rawObj;
    return true;
  }
  return false;
}

bool MLX90614::readAmbient() {
  int16_t rawAmb;
  // Read from the TA register, store value in rawAmb
  if (I2CReadWord(MLX90614_REGISTER_TA, &rawAmb)) {
    _rawAmbient = rawAmb;
    return true;
  }
  return false;
}

bool MLX90614::readMax() {
  int16_t toMax;
  // Read from the TOMax EEPROM address, store value in toMax
  if (I2CReadWord(MLX90614_REGISTER_TOMAX, &toMax)) {
    _rawMax = toMax;
    return true;
  }
  return false;
}

bool MLX90614::readMin() {
  int16_t toMin;
  // Read from the TOMin EEPROM address, store value in toMax
  if (I2CReadWord(MLX90614_REGISTER_TOMIN, &toMin))	{
    _rawMin = toMin;
    return true;
  }
  return false;
}

uint8_t MLX90614::setMax(float maxTemp) {
  // Convert the unit-ed value to a raw ADC value:
  int16_t rawMax = calcRawTemp(maxTemp);
  // Write that value to the TOMAX EEPROM address:
  return writeEEPROM(MLX90614_REGISTER_TOMAX, rawMax);
}

uint8_t MLX90614::setMin(float minTemp) {
  // Convert the unit-ed value to a raw ADC value:
  int16_t rawMin = calcRawTemp(minTemp);
  // Write that value to the TOMIN EEPROM address:
  return writeEEPROM(MLX90614_REGISTER_TOMIN, rawMin);
}

uint8_t MLX90614::setEmissivity(float emis) {
  // Make sure emissivity is between 0.1 and 1.0
  if ((emis > 1.0) || (emis < 0.1)) {
    return false;
  }
  // Calculate the raw 16-bit value:
  uint16_t ke = uint16_t(65535.0 * emis);
  ke = constrain(ke, 0x2000, 0xFFFF);

  // Write that value to the ke register
  return writeEEPROM(MLX90614_REGISTER_KE, (int16_t)ke);
}

float MLX90614::readEmissivity() {
  int16_t ke;
  if (I2CReadWord(MLX90614_REGISTER_KE, &ke)) {
    // If we successfully read from the ke register
    // calculate the emissivity between 0.1 and 1.0:
    return (((float)((uint16_t)ke)) / 65535.0);
  }
  return false;
}

uint8_t MLX90614::readI2CAddress() {
  int16_t tempAdd;
  // Read from the 7-bit I2C address EEPROM storage address:
  if (I2CReadWord(MLX90614_REGISTER_ADDRESS, &tempAdd))	{
    // If read succeeded, return the address:
    return (uint8_t) tempAdd;
  }
  return false; // Else return fail
}

bool MLX90614::setI2CAddress(uint8_t newAdd) {
  int16_t tempAdd;
  // Make sure the address is within the proper range:
  if ((newAdd >= 0x80) || (newAdd == 0x00)) {
    return false; // Return fail if out of range
  }
  // Read from the I2C address address first:
  if (I2CReadWord(MLX90614_REGISTER_ADDRESS, &tempAdd)) {
    tempAdd &= 0xFF00; // Mask out the address (MSB is junk?)
    tempAdd |= newAdd; // Add the new address

    // Write the new address back to EEPROM:
    return writeEEPROM(MLX90614_REGISTER_ADDRESS, tempAdd);
  }
  return true;
}

uint8_t MLX90614::readId() {
  for (int i = 0; i < 4; i++)	{
    int16_t temp = 0;
    // Read from all four ID registers, beginning at the first:
    if (!I2CReadWord(MLX90614_REGISTER_ID0 + i, &temp)) {
      return false;
    }
    // If the read succeeded, store the ID into the id array:
    id[i] = (uint16_t)temp;
  }
  return true;
}

uint32_t MLX90614::getIdHigh() {
  // Return the upper 32 bits of the ID
  return ((uint32_t)id[3] << 16) | id[2];
}

uint32_t MLX90614::getIdLow() {
  // Return the lower 32 bits of the ID
  return ((uint32_t)id[1] << 16) | id[0];
}

int16_t MLX90614::readReg(byte reg) {
  int16_t reg_cnt;
  if (!I2CReadWord(reg, &reg_cnt)) {
    Serial.println("failed to read reg");
    return -1;
  }

  return reg_cnt;
}

int16_t MLX90614::calcRawTemp(float calcTemp) {
  int16_t rawTemp; // Value to eventually be returned

  if (_defaultUnit == TEMP_RAW)	{
    // If unit is set to raw, just return that:
    rawTemp = (int16_t) calcTemp;
  }
  else {
    float tempFloat;
    // First convert each temperature to Kelvin:
    if (_defaultUnit == TEMP_F)	{
      // Convert from farenheit to Kelvin
      tempFloat = (calcTemp - 32.0) * 5.0 / 9.0 + 273.15;
    }
    else if (_defaultUnit == TEMP_C) {
      tempFloat = calcTemp + 273.15;
    }
    else if (_defaultUnit == TEMP_K) {
      tempFloat = calcTemp;
    }
    // Then multiply by 0.02 degK / bit
    tempFloat *= 50;
    rawTemp = (int16_t) tempFloat;
  }
  return rawTemp;
}

float MLX90614::calcTemperature(int16_t rawTemp) {
  float retTemp;

  if (_defaultUnit == TEMP_RAW)	{
    retTemp = (float) rawTemp;
  }
  else {
    retTemp = float(rawTemp) * 0.02;
    if (_defaultUnit != TEMP_K) {
      retTemp -= 273.15;
      if (_defaultUnit == TEMP_F) {
        retTemp = retTemp * 9.0 / 5.0 + 32;
      }
    }
  }

  return retTemp;
}

bool MLX90614::I2CReadWord(byte reg, int16_t * dest) {
  _wire->beginTransmission(_deviceAddress);
  _wire->write(reg);

  _wire->endTransmission(false); // Send restart
  _wire->requestFrom(_deviceAddress, (uint8_t) 3);

  uint8_t lsb = _wire->read();
  uint8_t msb = _wire->read();
  uint8_t pec = _wire->read();

  uint8_t crc = crc8(0, (_deviceAddress << 1));
  crc = crc8(crc, reg);
  crc = crc8(crc, (_deviceAddress << 1) + 1);
  crc = crc8(crc, lsb);
  crc = crc8(crc, msb);

  if (crc == pec)	{
    *dest = (msb << 8) | lsb;
    return true;
  }
  else {
    return false;
  }
}

bool MLX90614::writeEEPROM(byte reg, int16_t data) {
  // Clear out EEPROM first:
  if (I2CWriteWord(reg, 0) != 0) {
    return false;
  }
  delay(5); // Delay to erase

  uint8_t i2cRet = I2CWriteWord(reg, data);
  delay(5); // Delay to write

  if (i2cRet == 0)
    return true;
  else
    return false;
}

uint8_t MLX90614::I2CWriteWord(byte reg, int16_t data) {
  uint8_t crc;
  uint8_t lsb = data & 0x00FF;
  uint8_t msb = (data >> 8);

  crc = crc8(0, (_deviceAddress << 1));
  crc = crc8(crc, reg);
  crc = crc8(crc, lsb);
  crc = crc8(crc, msb);

  _wire->beginTransmission(_deviceAddress);
  _wire->write(reg);
  _wire->write(lsb);
  _wire->write(msb);
  _wire->write(crc);
  return _wire->endTransmission(true);
}

uint8_t MLX90614::crc8 (uint8_t inCrc, uint8_t inData) {
  uint8_t i;
  uint8_t data;
  data = inCrc ^ inData;
  for ( i = 0; i < 8; i++ )
  {
    if (( data & 0x80 ) != 0 )
    {
      data <<= 1;
      data ^= 0x07;
    }
    else
    {
      data <<= 1;
    }
  }
  return data;
}
