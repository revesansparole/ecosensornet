#include <Wire.h>

#include "sensor_mlx90614.h"

MLX90614 sensor;

void setup()
{
//  pinMode(Vext, OUTPUT);
//  digitalWrite(Vext, LOW);
  delay(1000);
  Serial.begin(115200);
  while (!Serial) {
  }

  Serial.println ("Thermal ...");

  Wire.begin(); //Joing I2C bus

  if (!sensor.begin(0x5A)) { // Initialize thermal IR sensor
    Serial.println("Qwiic IR thermometer did not acknowledge! Freezing!");
    while (true);
  }
  Serial.println("Qwiic IR Thermometer did acknowledge.");

  sensor.setUnit(TEMP_C);
}

void loop()
{

  if (sensor.read())  {
    Serial.println("Ambient: " + String(sensor.ambient(), 2) + " Object: " + String(sensor.object(), 2) + " [°C]");
  }

  Serial.println();
  delay(1000);
}
