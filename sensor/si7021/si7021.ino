#include <Wire.h>

#include "sensor_si7021.h"

SI7021 sensor = SI7021();

void setup() {
  Serial.begin(115200);

  // Vext ON
  pinMode(Vext, OUTPUT);
  digitalWrite(Vext, LOW);

  Wire.begin(); //Joing I2C bus

  delay(50);

  if (!sensor.begin()) {
    Serial.println("Did not find Si7021 sensor!");
    while (true) {}
  }

  Serial.print("Found model ");
  switch (sensor.getModel()) {
    case SI_Engineering_Samples:
      Serial.print("SI engineering samples");
      break;
    case SI_7013:
      Serial.print("Si7013");
      break;
    case SI_7020:
      Serial.print("Si7020");
      break;
    case SI_7021:
      Serial.print("Si7021");
      break;
    case SI_UNKNOWN:
    default:
      Serial.print("Unknown");
  }
  Serial.print(" Rev(");
  Serial.print(sensor.getRevision());
  Serial.print(")");
  Serial.print(" Serial #");
  Serial.print(sensor.sernum_a, HEX);
  Serial.println(sensor.sernum_b, HEX);
  Serial.println();

}

void loop() {

  Serial.println("Humidity: " + String(sensor.readHumidity(), 2) + " [%]");
  Serial.println("\tTemperature: " + String(sensor.readTemperature(), 2) + " [°C]");

  Serial.println();
  delay(1000);
}
