#include "sensor_si7021.h"

SI7021::SI7021() {
  sernum_a = sernum_b = 0;
  _model = SI_7021;
  _revision = 0;
}

/*!
    @brief  Sets up the HW by reseting It, reading serial number and reading
   revision.
    @return true if set up is successful.
*/
bool SI7021::begin() {
  Wire.beginTransmission(SI7021_I2CADDR);
  if (Wire.endTransmission())
    return false; // device not available at the expected address

  reset();
  if (_read8(SI7021_READRHT_REG_CMD) != SI7021_RHT_REG_DEFAULT)
    return false;

  readSerialNumber();
  _readRevision();

  return true;
}

/*!
    @brief  Reads the humidity value from Si7021 (No Master hold)
    @return Returns humidity as float value or NAN when there is error timeout
*/
float SI7021::readHumidity() {
  Wire.beginTransmission(SI7021_I2CADDR);

  Wire.write(SI7021_MEASRH_NOHOLD_CMD);
  uint8_t err = Wire.endTransmission();

  if (err != 0)
    return NAN; // error

  delay(20); // account for conversion time for reading humidity

  uint32_t start = millis(); // start timeout
  while (millis() - start < _TRANSACTION_TIMEOUT) {
    if (Wire.requestFrom(SI7021_I2CADDR, 3) == 3) {
      uint16_t hum = Wire.read() << 8 | Wire.read();
      uint8_t chxsum = Wire.read();

      float humidity = hum * 125. / 65536 - 6;

      return humidity;
    }
    delay(6); // 1/2 typical sample processing time
  }
  return NAN; // Error timeout
}

/*!
    @brief  Reads the temperature value from Si7021 (No Master hold)
    @return Returns temperature as float value or NAN when there is error
   timeout
*/
float SI7021::readTemperature() {
  Wire.beginTransmission(SI7021_I2CADDR);
  Wire.write(SI7021_MEASTEMP_NOHOLD_CMD);
  uint8_t err = Wire.endTransmission();

  if (err != 0)
    return NAN; // error

  delay(20); // account for conversion time for reading temperature

  uint32_t start = millis(); // start timeout
  while (millis() - start < _TRANSACTION_TIMEOUT) {
    if (Wire.requestFrom(SI7021_I2CADDR, 3) == 3) {
      uint16_t temp = Wire.read() << 8 | Wire.read();
      uint8_t chxsum = Wire.read();

      float temperature = temp * 175.72 / 65536 - 46.85;

      return temperature;
    }
    delay(6); // 1/2 typical sample processing time
  }

  return NAN; // Error timeout
}

/*!
    @brief  Sends the reset command to Si7021.
*/
void SI7021::reset() {
  Wire.beginTransmission(SI7021_I2CADDR);
  Wire.write(SI7021_RESET_CMD);
  Wire.endTransmission();
  delay(50);
}

void SI7021::_readRevision(void) {
  Wire.beginTransmission(SI7021_I2CADDR);
  Wire.write((uint8_t)(SI7021_FIRMVERS_CMD >> 8));
  Wire.write((uint8_t)(SI7021_FIRMVERS_CMD & 0xFF));
  Wire.endTransmission();

  uint32_t start = millis(); // start timeout
  while (millis() - start < _TRANSACTION_TIMEOUT) {
    if (Wire.requestFrom(SI7021_I2CADDR, 2) == 2) {
      uint8_t rev = Wire.read();
      Wire.read();

      if (rev == SI7021_REV_1) {
        rev = 1;
      } else if (rev == SI7021_REV_2) {
        rev = 2;
      }
      _revision = rev;
      return;
    }
    delay(2);
  }
  _revision = 0;
  return; // Error timeout
}

/*!
    @brief  Reads serial number and stores It in sernum_a and sernum_b variable
*/
void SI7021::readSerialNumber() {
  Wire.beginTransmission(SI7021_I2CADDR);
  Wire.write((uint8_t)(SI7021_ID1_CMD >> 8));
  Wire.write((uint8_t)(SI7021_ID1_CMD & 0xFF));
  Wire.endTransmission();

  bool gotData = false;
  uint32_t start = millis(); // start timeout
  while (millis() - start < _TRANSACTION_TIMEOUT) {
    if (Wire.requestFrom(SI7021_I2CADDR, 8) == 8) {
      gotData = true;
      break;
    }
    delay(2);
  }
  if (!gotData)
    return; // error timeout

  sernum_a = Wire.read();
  Wire.read();
  sernum_a <<= 8;
  sernum_a |= Wire.read();
  Wire.read();
  sernum_a <<= 8;
  sernum_a |= Wire.read();
  Wire.read();
  sernum_a <<= 8;
  sernum_a |= Wire.read();
  Wire.read();

  Wire.beginTransmission(SI7021_I2CADDR);
  Wire.write((uint8_t)(SI7021_ID2_CMD >> 8));
  Wire.write((uint8_t)(SI7021_ID2_CMD & 0xFF));
  Wire.endTransmission();

  gotData = false;
  start = millis(); // start timeout
  while (millis() - start < _TRANSACTION_TIMEOUT) {
    if (Wire.requestFrom(SI7021_I2CADDR, 8) == 8) {
      gotData = true;
      break;
    }
    delay(2);
  }
  if (!gotData)
    return; // error timeout

  sernum_b = Wire.read();
  Wire.read();
  sernum_b <<= 8;
  sernum_b |= Wire.read();
  Wire.read();
  sernum_b <<= 8;
  sernum_b |= Wire.read();
  Wire.read();
  sernum_b <<= 8;
  sernum_b |= Wire.read();
  Wire.read();

  switch (sernum_b >> 24) {
    case 0:
    case 0xff:
      _model = SI_Engineering_Samples;
      break;
    case 0x0D:
      _model = SI_7013;
      break;
    case 0x14:
      _model = SI_7020;
      break;
    case 0x15:
      _model = SI_7021;
      break;
    default:
      _model = SI_UNKNOWN;
  }
}

/*!
    @brief  Returns sensor model established during init
    @return model value
*/
si_sensorType SI7021::getModel() {
  return _model;
}

/*!
    @brief  Enable/Disable sensor heater
*/
void SI7021::heater(bool h) {
  uint8_t regValue = _read8(SI7021_READRHT_REG_CMD);

  if (h) {
    regValue |= (1 << (SI7021_REG_HTRE_BIT));
  } else {
    regValue &= ~(1 << (SI7021_REG_HTRE_BIT));
  }
  _write8(SI7021_WRITERHT_REG_CMD, regValue);
}

/*!
    @brief  Return sensor heater state
    @return heater state (TRUE = enabled, FALSE = disabled)
*/
bool SI7021::isHeaterEnabled() {
  uint8_t regValue = _read8(SI7021_READRHT_REG_CMD);
  return (bool)bitRead(regValue, SI7021_REG_HTRE_BIT);
}

/*!
    @brief  Set the sensor heater heat level
*/
void SI7021::setHeatLevel(uint8_t level) {
  _write8(SI7021_WRITEHEATER_REG_CMD, level);
}

/*******************************************************************/

void SI7021::_write8(uint8_t reg, uint8_t value) {
  Wire.beginTransmission(SI7021_I2CADDR);
  Wire.write(reg);
  Wire.write(value);
  Wire.endTransmission();
}


uint8_t SI7021::_read8(uint8_t reg) {
  uint8_t value;
  Wire.beginTransmission(SI7021_I2CADDR);
  Wire.write((uint8_t)reg);
  Wire.endTransmission();

  uint32_t start = millis(); // start timeout
  while (millis() - start < _TRANSACTION_TIMEOUT) {
    if (Wire.requestFrom(SI7021_I2CADDR, 1) == 1) {
      value = Wire.read();
      return value;
    }
    delay(2);
  }

  return 0; // Error timeout
}


uint16_t SI7021::_read16(uint8_t reg) {
  uint16_t value;
  Wire.beginTransmission(SI7021_I2CADDR);
  Wire.write(reg);
  Wire.endTransmission(false);

  uint32_t start = millis(); // start timeout
  while (millis() - start < _TRANSACTION_TIMEOUT) {
    if (Wire.requestFrom(SI7021_I2CADDR, 2) == 2) {
      value = Wire.read() << 8 | Wire.read();
      return value;
    }
    delay(2);
  }
  return 0; // Error timeout
}
