#ifndef SENSOR_MAX6675_H
#define SENSOR_MAX6675_H

#include "Arduino.h"

class MAX6675 {
  public:
    MAX6675(uint8_t sclk_pin, uint8_t cs_pin, uint8_t miso_pin);
    void begin();

    float read_celsius(void);

  private:
    uint8_t _sclk;
    uint8_t _miso;
    uint8_t _cs;
    
    uint8_t spiread(void);
};


#endif //  SENSOR_MAX6675_H
