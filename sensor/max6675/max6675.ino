#include "sensor_max6675.h"

int thermoDO = 13; // D7
int thermoCS = 12; // D6
int thermoCLK = 14;  // D5

MAX6675 th = MAX6675(thermoCLK, thermoCS, thermoDO);

void setup() {
  Serial.begin(115200);
  delay(300);

  th.begin();

  Serial.println("setup");

}

void loop() {
  Serial.print("C = ");
  Serial.println(th.read_celsius());

  delay(1000);
}
