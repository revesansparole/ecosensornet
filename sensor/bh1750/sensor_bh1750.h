#ifndef SENSOR_BH1750_H
#define SENSOR_BH1750_H

#include "Arduino.h"
#include "Wire.h"

#define BH1750_DEBUG 0

#define BH1750_I2CADDR 0x23  // 0x5c if addr is high

#define BH1750_ONE_TIME_H_RESOLUTION 0x20
#define BH1750_MTREG_DEFAULT 0x45


class BH1750 {
  public:
    BH1750();
    void begin(uint8_t address = BH1750_I2CADDR, TwoWire &wirePort = Wire);
    uint8_t address();
    bool isConnected();
    float readLight();

  private:
    uint8_t _deviceAddress; // 7-bit I2C address
    TwoWire *_wire;

};


#endif //  SENSOR_BH1750_H
