#include "sensor_bh1750.h"

BH1750::BH1750() {
}


void BH1750::begin(uint8_t address, TwoWire &wirePort) {
  _deviceAddress = address;
  _wire = &wirePort;
}

// I2C address of device
uint8_t BH1750::address() {
  return _deviceAddress;
}

// check connection to device
bool BH1750::isConnected() {
  _wire->beginTransmission(_deviceAddress);
  byte resp = _wire->endTransmission();
#if (BH1750_DEBUG == 1)
  Serial.println("end transmission" + String(resp));
#endif
  
  return resp == 0;
}

float BH1750::readLight() {
  byte ack = 0;
  float sensitivity = 69.;

  _wire->beginTransmission(_deviceAddress);
  _wire->write(BH1750_ONE_TIME_H_RESOLUTION);
  ack = _wire->endTransmission();
#if (BH1750_DEBUG == 1)
  Serial.print("ACK write: ");
  Serial.println(ack, DEC);
#endif

  delay(200);  // wait for measurement

  // read measurement
  unsigned int meas_count = 0;

  ack = _wire->requestFrom(_deviceAddress, (uint8_t)2);
#if (BH1750_DEBUG == 1)
  Serial.print("ACK read: ");
  Serial.println(ack, DEC);
#endif
  meas_count = _wire->read();
  meas_count <<= 8;
  meas_count |= _wire->read();

  return meas_count * (BH1750_MTREG_DEFAULT / sensitivity) / 1.2;
}
