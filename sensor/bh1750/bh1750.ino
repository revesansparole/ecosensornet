#include <Wire.h>

#include "sensor_bh1750.h"

BH1750 sensor;

void setup() {

  //Vext ON
  pinMode(Vext, OUTPUT);
  digitalWrite(Vext, LOW);

  Serial.begin(115200);

  Wire.begin(); //Join I2C bus

  delay(1000);
  sensor.begin(0x23);  // 0x23 default, 0x5c if addr is high
  while (!sensor.isConnected()) {
    Serial.println("Could not find a valid BH1750 sensor @" + String(sensor.address()) + ", check wiring!");
    delay(1000);
  }
}

void loop() {

  Serial.println("Light: " + String(sensor.readLight(), 2) + " [lx]");

  Serial.println();
  delay(1000);

}
