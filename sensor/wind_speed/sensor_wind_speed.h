#ifndef SENSOR_WIND_SPEED_H
#define SENSOR_WIND_SPEED_H

#include "Arduino.h"

#define WIND_SPEED_CAL 2.4  // [km.h-1.(#.s-1)-1] factor to convert rotation speed into wind speed

/* This object uses an interrupt to count anemometer rotation
    and therefore only one instance of it can be created

*/
class WindSpeed
{
  public:
    WindSpeed(int input_pin);
    boolean begin();

    float getWindSpeed();
    void reset();

    static void count_anemometer_rotation();

  private:
    int _input_pin;
    unsigned long _last_reset_time;

};

#endif // SENSOR_WIND_SPEED_H
