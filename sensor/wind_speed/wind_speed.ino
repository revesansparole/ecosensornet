#include "sensor_wind_speed.h"

WindSpeed sensor(GPIO2);

void setup() {
  Serial.begin(115200);

  if (!sensor.begin()) {
    Serial.println("Problem with wind speed sensor, check wiring!");
    while (true) {}
  }
}

void loop() {
  Serial.println("Average speed: " + String(sensor.getWindSpeed(), 2) + " [m.s-1]");
  Serial.println();
  
  sensor.reset();

  delay(2000);
}
