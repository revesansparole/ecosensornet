#include "Arduino.h"
#include "sensor_wind_speed.h"

#define DEBOUNCE_TIME 15
#define MAX_UNSIGNED_LONG 4294967295

volatile int anemometer_rotation_count;
volatile unsigned long ws_last_micro_rg;


WindSpeed::WindSpeed(int input_pin) {
  _input_pin = input_pin;
}

boolean WindSpeed::begin() {
  pinMode(_input_pin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(_input_pin), count_anemometer_rotation, FALLING);

  reset();

  return true;
}

//Returns the ammount of rain since the last time the getRain function was called.
void WindSpeed::reset() {
  anemometer_rotation_count = 0;
  _last_reset_time = millis();
}

// Returns average wind speed since last reset.
float WindSpeed::getWindSpeed() {
  float elapsed = 0;
  unsigned long current = millis();
  if (current > _last_reset_time) {
    elapsed = (current - _last_reset_time) / 1e3;
  } else {  // overflow of milis function
    elapsed = (MAX_UNSIGNED_LONG - _last_reset_time + current) / 1e3;
  }
  return anemometer_rotation_count / elapsed * WIND_SPEED_CAL;
}

// ISR for wind speed, count number of rotation of anemometer
void WindSpeed::count_anemometer_rotation() {
  if ((long)(micros() - ws_last_micro_rg) >= DEBOUNCE_TIME * 1000)	{
    anemometer_rotation_count++;
    ws_last_micro_rg = micros();
  }
}
