#include <Wire.h>

#include "sensor_aht21.h"

AHT21 sensor;

void setup() {
    Serial.begin(115200);
    Wire.begin();
  
    //Vext ON
    pinMode(Vext, OUTPUT);
    digitalWrite(Vext, LOW);
  
    delay(500);
    while (!Serial) {
    }
  
    Serial.println ("aht21 ...");
  
    if (!sensor.begin()) {
      Serial.println("AHT21 sensor did not acknowledge! Freezing!");
      while (true);
    }
    Serial.println("AHT21 sensor did acknowledge.");
}

void loop() {
  sensor.read();
  Serial.println("temperature: " + String(sensor.temperature()) + " [°C]");
  Serial.println("humidity: " + String(sensor.humidity()) + " [%]");

  delay(1000);
}
