#ifndef SENSOR_AHT21_H
#define SENSOR_AHT21_H

#include "Arduino.h"
#include "Wire.h"

#define AHT21_DEBUG 0

#define AHT21_I2CADDR 0x38


class AHT21 {
  public:
    AHT21();
    bool begin(byte address = AHT21_I2CADDR, TwoWire &wirePort = Wire);
    byte address();
    bool isConnected();

    // read() pulls the latest ambient and object temperatures from the
    // MLX90614. It will return either 1 on success or 0 on failure. (Failure
    // can result from either a timed out I2C transmission, or an incorrect
    // checksum value).
    bool read(void);

    // temperature() returns the sensor's most recently read ambient temperature
    // after the read() function has returned successfully. The float value
    // returned will be in [°C].
    float temperature(void);

    // humidity() returns the sensor's most recently read ambient humidity
    // after the read() function has returned successfully. The float value
    // returned will be in the units [%].
    float humidity(void);

  private:
    byte _deviceAddress; // 7-bit I2C address
    TwoWire *_wire;
    
    // These keep track of the raw values read from the sensor:
    unsigned long _rawTemperature;  // ambient air temperature
    unsigned long _rawHumidity;  // ambient air relative humidity

    byte status();

};


#endif //  SENSOR_AHT21_H
