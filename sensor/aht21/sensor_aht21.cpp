#include "sensor_aht21.h"

byte cmd_measure[3]   = {0xAC, 0x33, 0x00};

AHT21::AHT21() {
}


bool AHT21::begin(byte address, TwoWire &wirePort) {
  _deviceAddress = address;
  _wire = &wirePort;

  return isConnected();
}

// I2C address of device
byte AHT21::address() {
  return _deviceAddress;
}

// check connection to device
bool AHT21::isConnected() {
  byte stat = status();
#if (AHT21_DEBUG == 1)
  Serial.println("status " + String(stat) + ", &0x18: " + String(stat & 0x18) + ", cmp: " + String((byte)(stat & 0x18) == 0x18));
#endif

  return (byte)(stat & 0x18) == 0x18;
}

bool AHT21::read(void) {
  _wire->beginTransmission(_deviceAddress);
  _wire->write(cmd_measure, 3);
  _wire->endTransmission();
  delay(80);

  while ((byte)(status() & 0x80) == 0x80) {
#if (AHT21_DEBUG == 1)
    Serial.println("waiting for measurement");
#endif
    delay(10);
  }

  unsigned long temp[6];
  _wire->requestFrom(_deviceAddress, 6);
  for (unsigned char i = 0; _wire->available() > 0; i++)
  {
    temp[i] = _wire->read();
#if (AHT21_DEBUG == 1)
    Serial.println("read: " + String(temp[i]) + " bla");
#endif
  }

  _rawHumidity = ((temp[1] << 16) | (temp[2] << 8) | temp[3]) >> 4;
  _rawTemperature = ((temp[3] & 0x0f) << 16) | (temp[4] << 8) | temp[5];
#if (AHT21_DEBUG == 1)
  Serial.println("raw: " + String(_rawTemperature) + ", rh: " + String(_rawHumidity));
#endif

}

float AHT21::temperature(void) {
  return ((float)_rawTemperature * 200 / 1048576) - 50;
}

float AHT21::humidity(void) {
  if (_rawHumidity == 0) {
    return 0;                       // Some unrealistic value
  }
  return (float)_rawHumidity * 100 / 1048576;
}

byte AHT21::status() {
  _wire->beginTransmission(_deviceAddress);
  _wire->write(0x71);
  _wire->endTransmission();

  _wire->requestFrom(_deviceAddress, 1);
  return _wire->read();
}
