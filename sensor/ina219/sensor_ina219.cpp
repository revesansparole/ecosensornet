#include "sensor_ina219.h"

INA219::INA219() {
  _wire = &Wire;
  i2cAddress = 0x40;
}


boolean INA219::begin() {
  Wire.begin();

  if ( !reset() ) {
    return false;
  }
  setADCMode(BIT_MODE_12);
  setMeasureMode(CONTINUOUS);
  setPGain(PG_320);
  setBusRange(BRNG_32);
  shuntFactor = 1.0;
  overflow = false;

  return true;
}

bool INA219::reset() {
  byte ack = writeRegister(INA219_CONF_REG, INA219_RST);
  return ack == 0;
}

void INA219::setADCMode(INA219_ADC_MODE mode) {
  deviceADCMode = mode;
  uint16_t currentConfReg = readRegister(INA219_CONF_REG);
  currentConfReg &= ~(0x0780);
  currentConfReg &= ~(0x0078);
  uint16_t adcMask = mode << 3;
  currentConfReg |= adcMask;
  adcMask = mode << 7;
  currentConfReg |= adcMask;
  writeRegister(INA219_CONF_REG, currentConfReg);
}

void INA219::setMeasureMode(INA219_MEASURE_MODE mode) {
  deviceMeasureMode = mode;
  uint16_t currentConfReg = readRegister(INA219_CONF_REG);
  currentConfReg &= ~(0x0007);
  currentConfReg |= deviceMeasureMode;
  writeRegister(INA219_CONF_REG, currentConfReg);
}

void INA219::setPGain(INA219_PGAIN gain) {
  devicePGain = gain;
  uint16_t currentConfReg = readRegister(INA219_CONF_REG);
  currentConfReg &= ~(0x1800);
  currentConfReg |= devicePGain;
  writeRegister(INA219_CONF_REG, currentConfReg);

  switch (devicePGain) {
    case PG_40:
      calVal = 20480;
      currentDivider_mA = 50.0;
      pwrMultiplier_mW = 0.4;
      shuntOverflowLimit = 4000;
      break;
    case PG_80:
      calVal = 10240;
      currentDivider_mA = 25.0;
      pwrMultiplier_mW = 0.8;
      shuntOverflowLimit = 8000;
      break;
    case PG_160:
      calVal = 8192;
      currentDivider_mA = 20.0;
      pwrMultiplier_mW = 1.0;
      shuntOverflowLimit = 16000;
      break;
    case PG_320:
      calVal = 4096;
      currentDivider_mA = 10.0;
      pwrMultiplier_mW = 2.0;
      shuntOverflowLimit = 32000;
      break;
  }

  writeRegister(INA219_CAL_REG, calVal);

}

void INA219::setBusRange(INA219_BUS_RANGE range) {
  deviceBusRange = range;
  uint16_t currentConfReg = readRegister(INA219_CONF_REG);
  currentConfReg &= ~(0x2000);
  currentConfReg |= deviceBusRange;
  writeRegister(INA219_CONF_REG, currentConfReg);
}

/*
   shuntSize: value of shunt in Ohms
*/
void INA219::setShuntSize(float shuntSize) {
  shuntFactor = shuntSize / 0.1;
}

/*
   shunt voltage: [mV]
*/
float INA219::getShuntVoltage() {
  int16_t val;
  val = (int16_t) readRegister(INA219_SHUNT_REG);

  if ((abs(val)) == shuntOverflowLimit) {
    overflow = true;
  }
  else {
    overflow = false;
  }
  return (val * 0.01);
}


/*
   bus voltage: [V] Tension on Vin-
*/
float INA219::getBusVoltage() {
  uint16_t val;
  val = readRegister(INA219_BUS_REG);
  val = ((val >> 3) * 4);
  return (val * 0.001);
}


/*
   current: [mA] current accross shunt
*/
float INA219::getCurrent() {
  int16_t val;
  val = (int16_t)readRegister(INA219_CURRENT_REG);
  return (val / (currentDivider_mA * shuntFactor));
}


/*
   power: [mW] power dissipatd in load
*/
float INA219::getBusPower() {
  uint16_t val;
  val = readRegister(INA219_PWR_REG);
  return (val * pwrMultiplier_mW / shuntFactor);
}


/*
   bool: whether ADC overflown
*/
bool INA219::getOverflow() {
  uint16_t val;
  val = readRegister(INA219_BUS_REG);
  if (val & 1) {
    overflow = true;
  }
  return overflow;
}

void INA219::startSingleMeasurement() {
  uint16_t val = readRegister(INA219_BUS_REG); // clears CNVR (Conversion Ready) Flag
  val = readRegister(INA219_CONF_REG);
  writeRegister(INA219_CONF_REG, val);
  uint16_t convReady = 0x0000;
  while (!convReady) {
    convReady = ((readRegister(INA219_BUS_REG)) & 0x0002); // checks if sampling is completed
  }
}

void INA219::powerDown() {
  confRegCopy = readRegister(INA219_CONF_REG);
  setMeasureMode(POWER_DOWN);
}

void INA219::powerUp() {
  writeRegister(INA219_CONF_REG, confRegCopy);
  delayMicroseconds(40);
}

/*********************************************************************/

uint8_t INA219::writeRegister(uint8_t reg, uint16_t val) {
  _wire->beginTransmission(i2cAddress);
  uint8_t lVal = val & 255;
  uint8_t hVal = val >> 8;
  _wire->write(reg);
  _wire->write(hVal);
  _wire->write(lVal);
  return _wire->endTransmission();
}

uint16_t INA219::readRegister(uint8_t reg) {
  uint8_t MSByte = 0, LSByte = 0;
  uint16_t regValue = 0;
  _wire->beginTransmission(i2cAddress);
  _wire->write(reg);
  _wire->endTransmission();
  _wire->requestFrom(i2cAddress, 2);
  if (_wire->available()) {
    MSByte = _wire->read();
    LSByte = _wire->read();
  }
  regValue = (MSByte << 8) + LSByte;
  return regValue;
}
