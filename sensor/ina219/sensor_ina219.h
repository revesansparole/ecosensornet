#ifndef SENSOR_INA219_H
#define SENSOR_INA219_H

#include "Arduino.h"
#include "Wire.h"

#define INA219_DEBUG 0

/* registers */
#define INA219_CONF_REG     0x00 //Configuration Register
#define INA219_SHUNT_REG    0x01 //Shunt Voltage Register
#define INA219_BUS_REG      0x02 //Bus Voltage Register
#define INA219_PWR_REG      0x03 //Power Register 
#define INA219_CURRENT_REG  0x04 //Current flowing through Shunt
#define INA219_CAL_REG      0x05 //Calibration Register 

/* parameters */
#define INA219_RST             0x8000

typedef enum INA219_ADC_MODE {
  // Mode *                        * Res / Samples *        * Conversion Time *
  BIT_MODE_9      = 0b00000000,  //  9 Bit Resolution         84 µs
  BIT_MODE_10     = 0b00000001,  //  10 Bit Resolution        148 µs
  BIT_MODE_11     = 0b00000010,  //  11 Bit Resolution        276 µs
  BIT_MODE_12     = 0b00000011,  //  12 Bit Resolution        532 µs
  SAMPLE_MODE_2   = 0b00001001,  //  Mean Value 2 samples     1.06 ms
  SAMPLE_MODE_4   = 0b00001010,  //  Mean Value 4 samples     2.13 ms
  SAMPLE_MODE_8   = 0b00001011,  //  Mean Value 8 samples     4.26 ms
  SAMPLE_MODE_16  = 0b00001100,  //  Mean Value 16 samples    8.51 ms
  SAMPLE_MODE_32  = 0b00001101,  //  Mean Value 32 samples    17.02 ms
  SAMPLE_MODE_64  = 0b00001110,  //  Mean Value 64 samples    34.05 ms
  SAMPLE_MODE_128 = 0b00001111   //  Mean Value 128 samples   68.10 ms
} ina219AdcMode;

typedef enum INA219_MEASURE_MODE {
  POWER_DOWN      = 0b00000000,  //  INA219 switched off
  TRIGGERED       = 0b00000011,  //  measurement on demand
  ADC_OFF         = 0b00000100,  //  Analog/Digital Converter switched off
  CONTINUOUS      = 0b00000111  //  Continuous measurements
} ina219MeasureMode;

typedef enum INA219_PGAIN {
  //  * Shunt Voltage Range *   * Max Current (if shunt is 0.1 ohms) *
  PG_40       = 0x0000,   //  * 40 mV                    0.4 A
  PG_80       = 0x0800,   //  * 80 mV                    0.8 A
  PG_160      = 0x1000,   //  * 160 mV                   1.6 A
  PG_320      = 0x1800    //  * 320 mV                   3.2 A
} ina219PGain;

typedef enum INA219_BUS_RANGE {
  BRNG_16         = 0x0000,  //  16V
  BRNG_32         = 0x2000,  //  32V
} ina219BusRange;



class INA219 {
  public:
    INA219();
    boolean begin();  // by default go highres
    bool reset();
    void setADCMode(INA219_ADC_MODE mode);
    void setMeasureMode(INA219_MEASURE_MODE mode);
    void setPGain(INA219_PGAIN gain);
    void setBusRange(INA219_BUS_RANGE range);
    void setShuntSize(float shuntSize);
    float getShuntVoltage();
    float getBusVoltage();
    float getCurrent();
    float getBusPower();
    bool getOverflow();
    void startSingleMeasurement();
    void powerDown();
    void powerUp();
    uint8_t writeRegister(uint8_t reg, uint16_t val);
    uint16_t readRegister(uint8_t reg);

  private:
    INA219_ADC_MODE deviceADCMode;
    INA219_MEASURE_MODE deviceMeasureMode;
    INA219_PGAIN devicePGain;
    INA219_BUS_RANGE deviceBusRange;
    TwoWire *_wire;
    int i2cAddress;
    uint16_t calVal;
    uint16_t confRegCopy;
    float shuntFactor;
    float currentDivider_mA;
    float pwrMultiplier_mW;
    bool overflow;
    uint16_t shuntOverflowLimit;
};


#endif //  SENSOR_INA219_H
