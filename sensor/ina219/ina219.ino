#include <Wire.h>

#include "sensor_ina219.h"

INA219 sensor = INA219();

void setup() {
  Serial.begin(115200);
  //Vext ON
  pinMode(Vext, OUTPUT);
  digitalWrite(Vext, LOW);

  Wire.begin(); //Joing I2C bus

  if (!sensor.begin()) {
    Serial.println("INA219 not connected!");
    while (1) {}
  }

  sensor.setADCMode(BIT_MODE_12);
  sensor.setMeasureMode(TRIGGERED);
  sensor.setPGain(PG_40);
  sensor.setBusRange(BRNG_16);
}

void loop() {
  sensor.startSingleMeasurement();

  Serial.println("Shunt Voltage = " + String(sensor.getShuntVoltage(), 2) + " [mV]");
  Serial.println("Bus Voltage = " + String(sensor.getBusVoltage(), 2) + " [V]");
  Serial.println("Current = " + String(sensor.getCurrent(), 2) + " [mA]");
  Serial.println("Bus Power = " + String(sensor.getBusPower(), 2) + " [mW]");
  if (sensor.getOverflow()) {
    Serial.println("Overflow! Choose higher PGAIN");
  }
  else {
    Serial.println("Values OK - no overflow");
  }

  Serial.println();
  delay(1000);
}
