#include <Wire.h>

#include "sensor_bmp180.h"

BMP180 sensor;

void setup() {
  Serial.begin(115200);

  //Vext ON
  pinMode(Vext, OUTPUT);
  digitalWrite(Vext, LOW);

  Wire.begin(); //Joing I2C bus

  if (!sensor.begin(BMP180_ULTRAHIGHRES)) {
    Serial.println("Could not find a valid BMP180 sensor, check wiring!");
    while (1) {}
  }
}

void loop() {
  Serial.println("Temperature = " + String(sensor.readTemperature(), 2) + " [°C]");
  Serial.println("Pressure = " + String(sensor.readPressure(), 2) + " [Pa]");

  // Calculate altitude assuming 'standard' barometric
  // pressure of 1013.25 millibar = 101325 Pascal
  Serial.println("Altitude = " + String(sensor.readAltitude(), 2) + " [m]");

  Serial.println("Pressure at sealevel (calculated) = " + String(sensor.readSealevelPressure(), 2) + " [Pa]");

  // you can get a more precise measurement of altitude
  // if you know the current sea level pressure which will
  // vary with weather and such. If it is 1015 millibars
  // that is equal to 101500 Pascals.
  Serial.println("Real altitude = " + String(sensor.readAltitude(101500), 2) + " [m]");

  Serial.println();
  delay(500);
}
