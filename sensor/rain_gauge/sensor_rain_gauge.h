#ifndef SENSOR_RAIN_GAUGE_H
#define SENSOR_RAIN_GAUGE_H

#include "Arduino.h"

#define RAIN_GAUGE_CAL 0.2794  // [mm] rain amount for each tip of the bucket

/* This object uses an interrupt to count bucket typing
    and therefore only one instance of it can be created

*/
class RainGauge
{
  public:
    RainGauge(int input_pin);
    boolean begin();

    float getRain();
    void reset();

    static void count_tipping();

  private:
    int _input_pin;

};

#endif // SENSOR_RAIN_GAUGE_H
