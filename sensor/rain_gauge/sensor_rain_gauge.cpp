#include "Arduino.h"
#include "sensor_rain_gauge.h"

#define DEBOUNCE_TIME 15

volatile int bucket_tip_count;
volatile unsigned long rain_last_micro_rg;


RainGauge::RainGauge(int input_pin) {
  _input_pin = input_pin;
}

boolean RainGauge::begin() {
  pinMode(_input_pin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(_input_pin), count_tipping, FALLING);

  reset();

  return true;
}

//Returns the ammount of rain since the last time the getRain function was called.
void RainGauge::reset() {
  bucket_tip_count = 0;
}

// Returns the ammount of rain since last reset.
float RainGauge::getRain() {
  return bucket_tip_count * RAIN_GAUGE_CAL;
}

// ISR for rain gauge, count umber of time bucket tips
void RainGauge::count_tipping() {
  if ((long)(micros() - rain_last_micro_rg) >= DEBOUNCE_TIME * 1000)	{
    bucket_tip_count++;
    rain_last_micro_rg = micros();
  }
}
