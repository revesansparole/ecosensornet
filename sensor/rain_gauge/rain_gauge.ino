#include "sensor_rain_gauge.h"

RainGauge sensor(GPIO3);

void setup() {
  Serial.begin(115200);

  if (!sensor.begin()) {
    Serial.println("Problem with rain gauge sensor, check wiring!");
    while (true) {}
  }
}

void loop() {
  Serial.println("Total Rain: " + String(sensor.getRain(), 2) + " [mm]");

  Serial.println();
  delay(1000);
}
