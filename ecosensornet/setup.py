# {# pkglts, pysetup.kwds
# format setup arguments
from setuptools import setup, find_packages

short_descr = "Network of sensors for ecology agronomy"
readme = open('README.rst').read()
history = open('HISTORY.rst').read()

# find packages
pkgs = find_packages('src')



setup_kwds = dict(
    name='ecosensornet',
    version="0.0.1",
    description=short_descr,
    long_description=readme + '\n\n' + history,
    author="revesansparole",
    author_email="revesansparole@gmail.com",
    url='https://gitlab.com/revesansparole/ecosensornet',
    license='cc_by_nc',
    zip_safe=False,

    packages=pkgs,
    
    package_dir={'': 'src'},
    setup_requires=[
        "pytest-runner",
        ],
    install_requires=[
        ],
    tests_require=[
        "pytest",
        "pytest-mock",
        ],
    entry_points={},
    keywords='',
    )
# #}
# change setup_kwds below before the next pkglts tag

setup_kwds['entry_points']['console_scripts'] = ['esnet = ecosensornet.tools.esnet:cli']

# do not change things below
# {# pkglts, pysetup.call
setup(**setup_kwds)
# #}
