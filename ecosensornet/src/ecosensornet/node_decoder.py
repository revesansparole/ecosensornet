"""
Base class for all nodes. Need to be subclassed to make sense of payload.
"""

from .lora_event import LoraEvent

registered_decoder = {}


def decoder_factory(event):
    try:
        return registered_decoder[event.api](event)
    except KeyError:
        raise NotImplementedError(f"unable to find decoder for api: {event.api}")


class Meta(type):
    def __new__(mcs, name, bases, dct):
        cls = super().__new__(mcs, name, bases, dct)
        registered_decoder[cls.api] = cls
        return cls


class LoraDecoder(metaclass=Meta):
    """Base class to decode the node specific part of payload
    """
    name = "base"
    api = 0
    descr = """nothing"""
    nb_samples = 1  # number of samples in payload

    def __init__(self, event):
        """Constructor

        Args:
            event (LoraEvent):
        """
        self._event = event

    def to_float(self, ind, precision=4):
        """convert 4 bytes to float in payload

        Args:
            ind (int): index of first byte
            precision (int): number of bytes (default 4)

        Returns:
            (float)
        """
        return LoraEvent.to_float(self._event.payload, ind, precision)

    def to_int(self, ind, precision=2):
        """convert 4 bytes to float in payload

        Args:
            ind (int): index of first byte
            precision (int): number of bytes (default 2)

        Returns:
            (int)
        """
        return LoraEvent.to_int(self._event.payload, ind, precision)

    def battery(self):
        """Measured battery voltage

        Returns:
            (int): [mV]
        """
        return self._event.battery()

    def to_record(self, ind):
        """Format sample

        Args:
            ind (int): index of sample

        Returns:
            (dict)
        """
        assert ind < self.nb_samples
        return dict()
