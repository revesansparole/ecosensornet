"""
Network of sensors for ecology agronomy
"""
# {# pkglts, src
# FYEO
# #}
# {# pkglts, version, after src
from . import version

__version__ = version.__version__
# #}
from . import decoders
from .lora_event import LoraEvent
from .node_decoder import decoder_factory
