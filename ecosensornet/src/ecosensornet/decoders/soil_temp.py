"""
Soil temp node 2x(light, temp).
"""
from datetime import timedelta

from ..node_decoder import LoraDecoder


class LeafTempDecoder(LoraDecoder):
    name = "soil_temp"
    api = 101
    descr = ("date: UTC\n"
             "light1: [lx]\n"
             "light2: [lx]\n"
             "temp1: [°C]\n"
             "temp2: [°C]\n"
             "vbat: [mV]\n")
    nb_samples = 6

    def light1(self, nb):
        """Measure of light

        Args:
            nb (int): index of measure

        Returns:
            (float): [lx]
        """
        return self.to_float(nb * 16)

    def light2(self, nb):
        """Measure of light

        Args:
            nb (int): index of measure

        Returns:
            (float): [lx]
        """
        return self.to_float(nb * 16 + 4)

    def temp1(self, nb):
        """Measure of temperature using thermo sensor

        Args:
            nb (int): index of measure

        Returns:
            (float): [°C]
        """
        return self.to_float(nb * 16 + 8)

    def temp2(self, nb):
        """Measure of temperature using thermo sensor

        Args:
            nb (int): index of measure

        Returns:
            (float): [°C]
        """
        return self.to_float(nb * 16 + 12)

    def to_record(self, ind):
        """Format sample

        Args:
            ind (int): index of sample

        Returns:
            (dict)
        """
        record = super().to_record(ind)
        record['date'] = self._event.date + timedelta(seconds=-10 * (5 - ind))
        record['vbat'] = self.battery()
        record['light1'] = self.light1(ind)
        record['light2'] = self.light2(ind)
        record['temp1'] = self.temp1(ind)
        record['temp2'] = self.temp2(ind)

        return record
