"""
Leaf temp node (light, temp, IR).
"""
from datetime import timedelta

from ..node_decoder import LoraDecoder


class LeafTempDecoder(LoraDecoder):
    name = "leaf_temp"
    api = 100
    descr = ("date: UTC\n"
             "ir_ambient: [°C] IR sensor temperature\n"
             "ir_object: [°C]\n"
             "light: [lx]\n"
             "temp: [°C]\n"
             "vbat: [mV]\n")
    nb_samples = 6

    def light(self, nb):
        """Measure of light

        Args:
            nb (int): index of measure

        Returns:
            (float): [lx]
        """
        return self.to_float(nb * 16)

    def ir_ambient(self, nb):
        """Measure of ambient temperature using IR sensor

        Args:
            nb (int): index of measure

        Returns:
            (float): [°C]
        """
        return self.to_float(nb * 16 + 4)

    def ir_object(self, nb):
        """Measure of object temperature using IR sensor

        Args:
            nb (int): index of measure

        Returns:
            (float): [°C]
        """
        return self.to_float(nb * 16 + 8)

    def temp(self, nb):
        """Measure of temperature using thermo sensor

        Args:
            nb (int): index of measure

        Returns:
            (float): [°C]
        """
        return self.to_float(nb * 16 + 12)

    def to_record(self, ind):
        """Format sample

        Args:
            ind (int): index of sample

        Returns:
            (dict)
        """
        record = super().to_record(ind)
        record['date'] = self._event.date + timedelta(seconds=-10 * (5 - ind))
        record['vbat'] = self.battery()
        record['light'] = self.light(ind)
        record['ir_ambient'] = self.ir_ambient(ind)
        record['ir_object'] = self.ir_object(ind)
        record['temp'] = self.temp(ind)

        return record
