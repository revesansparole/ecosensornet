"""
Base class for all lora events.
"""
import json
import struct
from datetime import datetime


class LoraEvent:

    def __init__(self, pth_name, data_raw):
        """Constructor

        Args:
            pth_name (str): name of event file
            data_raw (bytes): content of file
        """
        dev_str, event_str = pth_name.split("/")
        event_data = json.loads(data_raw)
        payload = json.loads(event_data['objectJSON'])['DecodeDataHex'].split(",")

        self.device = dev_str
        self.date = datetime.fromisoformat(event_str[6:-5].replace("_", ":"))
        self.api = self.to_int(payload, 0, 2)

        self._battery = self.to_int(payload, 2, 2)

        self.payload = payload[4:]

    @staticmethod
    def to_float(data, ind, precision=4):
        """convert 4 bytes to float in payload

        Args:
            data (list): list of bytes
            ind (int): index of first byte
            precision (int): number of bytes (default 4)

        Returns:
            (float)
        """
        if precision == 4:
            fmt = 'f'
        elif precision == 8:
            fmt = 'd'
        else:
            raise NotImplementedError(f"unknown precision: {precision:d}")

        val, = struct.unpack(f'<{fmt}', bytes(int(v, 0) for v in data[ind:ind + precision]))
        return val

    @staticmethod
    def to_int(data, ind, precision=2):
        """convert 4 bytes to float in payload

        Args:
            data (list): list of bytes
            ind (int): index of first byte
            precision (int): number of bytes (default 2)

        Returns:
            (int)
        """
        if precision == 2:
            fmt = 'h'
        elif precision == 4:
            fmt = 'i'
        elif precision == 8:
            fmt = 'q'
        else:
            raise NotImplementedError(f"unknown precision: {precision:d}")

        val, = struct.unpack(f'<{fmt}', bytes(int(v, 0) for v in data[ind:ind + precision]))
        return val

    def battery(self):
        """Measured battery voltage

        Returns:
            (int): [mV]
        """
        return self._battery
