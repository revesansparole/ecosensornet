"""
Upload all data files on microcontrollers
"""
import shutil
from pathlib import Path
from subprocess import run

board_pth = Path("C:/Users/jchopard.DOMSUNR/AppData/Local/Arduino15/packages/esp8266")
assert board_pth.exists()

tool_pth = board_pth / "tools/mklittlefs/3.0.4-gcc10.3-1757bed/mklittlefs.exe"
assert tool_pth.exists()

python_pth = board_pth / "tools/python3/3.7.2-post1/python3.exe"
assert python_pth.exists()

upload_script = board_pth / "hardware/esp8266/3.0.2/tools/upload.py"
assert upload_script.exists()


def upload_data(port):
    """upload all data files in ./src/**

    Args:
        port (str): Serial port

    Returns:
        None
    """
    # collect all data files in single temporary directory
    temp_dir = Path("data_tmp")
    if temp_dir.exists():
        for pth in temp_dir.glob("*.*"):
            pth.unlink()
    else:
        temp_dir.mkdir()

    assert len(list(temp_dir.glob("*.*"))) == 0

    for pth in Path("src").glob("**/*.*"):
        if pth.suffix in (".json", ".css", ".html"):
            print(pth)
            shutil.copy(pth, temp_dir)

    # create image
    data_pth = temp_dir.absolute()
    print("data pth", data_pth)

    spi_page = 256
    spi_block = 4096
    spi_size = 64 * 1024

    img_pth = Path("data_img.mklittlefs.bin").absolute()
    if img_pth.exists():
        print("rm", img_pth)
        img_pth.unlink()

    # line 306
    cmd = [tool_pth, "-d", str(5), "-c", data_pth, "-p", str(spi_page), "-b", str(spi_block), "-s", str(spi_size),
           img_pth]
    ret = run(cmd)

    print("mklittlefs", ret)

    # upload image
    upload_speed = 115200
    upload_address = "0xEB000"

    # line 336
    cmd = [python_pth, upload_script,
           "--chip", "esp8266",
           "--port", port, "--baud", str(upload_speed),
           "write_flash", upload_address, img_pth]

    ret = run(cmd)
    print("upload", ret)

    # cleanup
    img_pth.unlink()
    for pth in temp_dir.glob("*.*"):
        pth.unlink()

    temp_dir.rmdir()
