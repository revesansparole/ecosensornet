"""
Copy template node files into current dir
"""
import shutil
from pathlib import Path

base = Path("C:/Users/jchopard.DOMSUNR/Desktop/perso/ecosensornet/node_base/base_esp8266")
assert base.exists()


def cp_tpl(dest='.', only_local=False):
    """Copy all files from node base

    Args:
        dest (str): Directory in which to copy files
        only_local (bool): Whether to copy all or only local files

    Returns:
        None
    """
    if dest == '.':
        dest = Path.cwd()
    else:
        dest = Path(dest)
        assert dest.exists()

    # ino file
    pth_ino = dest / f"{dest.name}.ino"
    if not pth_ino.exists():
        if only_local:
            code = """#include <LittleFS.h>

#include "src/calibration/calibration.h"
#include "src/measure/node_measure.h"

unsigned long t0;

void setup() {
  Serial.begin(115200);
  while (!Serial) {
    delay(10);
  }
  delay(300);

  LittleFS.begin();
  calibration::setup();
  measure::setup();
}

void loop() {
  String msg = "";

  t0 = millis();
  measure::sample();
  measure::fmt_config(msg);

  Serial.println("elapsed: " + String(millis() - t0));
  Serial.println(msg);

  delay(1000);
}
"""
            pth_ino.write_text(code)
        else:
            pth_ino_ref = base / "base_esp8266.ino"
            shutil.copy(pth_ino_ref, pth_ino)

    # all src files
    if only_local:
        shutil.copytree(base / "src/calibration", dest / "src/calibration", dirs_exist_ok=True)
        shutil.copytree(base / "src/measure", dest / "src/measure", dirs_exist_ok=True)
        shutil.copy(base / "src/settings.h", dest / "src")
    else:
        shutil.copytree(base / "src", dest / "src", dirs_exist_ok=True)
