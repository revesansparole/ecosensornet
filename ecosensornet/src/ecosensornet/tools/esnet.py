"""
Chapeau for all tools in ecosensornet
"""
from argparse import ArgumentParser

from .copy_template import cp_tpl
from .upload_data import upload_data


def cli():
    """Run CLI evaluation"""
    action = dict(
        tpl=cp_tpl,
        up=upload_data
    )
    # parse argument line
    parser = ArgumentParser(description="Ecosensornet tool manager")
    parser.add_argument("-v", "--verbosity", action="count", default=0,
                        help="increase output verbosity")

    subparsers = parser.add_subparsers(dest='subcmd', help='sub-command help')

    parser_tpl = subparsers.add_parser('tpl', help=cp_tpl.__doc__)
    parser_tpl.add_argument("-d", "--dest", default=".",
                            help="Directory in which to copy files")
    parser_tpl.add_argument("--local", dest='only_local', default=False, action='store_true',
                            help="Copy all or only local files")

    parser_up = subparsers.add_parser('up', help=upload_data.__doc__)
    parser_up.add_argument("-p", "--port", default="COM14",
                           help="Serial port to use to communicate with board")

    kwds = vars(parser.parse_args())
    print("verbosity", kwds.pop('verbosity'))

    # perform action
    subcmd = kwds.pop('subcmd')
    action[subcmd](**kwds)
