"""
Plot evolution of measures for single sensor
"""
import sys

import matplotlib.pyplot as plt
import pandas as pd

sid = sys.argv[1]

# read sensor
pth = f"res/events_dev_ff_00_00_00_00_00_{sid}.csv"
df = pd.read_csv(pth, sep=";", comment="#", parse_dates=["date"], index_col=['date'])

# plot evolution of all measured variables
fig, axes = plt.subplots(len(df.columns), 1, sharex='all', figsize=(14, 9), squeeze=False)

for i, vname in enumerate(df.columns):
    ax = axes[i, 0]
    ax.plot(df.index, df[vname])
    ax.set_ylabel(vname)

fig.tight_layout()
plt.show()
