"""
Plot evolution of battery voltage for all sensors
"""
from pathlib import Path

import matplotlib.pyplot as plt
import pandas as pd

dfs = []
for pth in Path("res").glob("events_dev_ff_00_00_00_00_00_*.csv"):
    df = pd.read_csv(pth, sep=";", comment="#", parse_dates=["date"], index_col=['date'])

    sid = "_".join(pth.name[:-4].split("_")[-2:])
    dfs.append((sid, df))

fig, axes = plt.subplots(1, 1, sharex='all', sharey='row', figsize=(14, 6), squeeze=False)
ax = axes[0, 0]

for sid, df in sorted(dfs):
    ax.plot(df.index, df['vbat'], label=sid)

ax.legend(loc='lower left')

fig.tight_layout()
plt.show()
