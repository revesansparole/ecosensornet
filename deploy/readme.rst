Deploy
======

Recipes to deploy the network.


set gateway server in UTC:
 - sudo timedatectl set-timezone UTC


RakGateway
==========

steps:

 - connect to "Rakwireless_XXXX" using rakwireless pwd
 - launch http://192.168.230.1:8080/ (admin admin)
 - Applications/myapp/integrations set http forward to http://localhost:8002
 - launch http://192.168.230.1:8020/menu to manage stored events

Always starts with synchronize clocks

connect in ssh:
 - ssh pi@192.168.230.1
 - pwd raspberry
