import json
from random import random

nb = 1

msg = dict(
    nid=1111,
    iid=2,
    interval=60,
    nb=nb,
    events=[dict(sensor1=random(), sensor2=random()) for i in range(nb)],
)

print(json.dumps(msg, indent=2))
