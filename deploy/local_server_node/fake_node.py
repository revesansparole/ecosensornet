"""
Fake node
=========

Small process running on localhost and emulating a node sending data regularly.

"""
from random import random

import requests

nb = 3

msg = dict(
    nid=1111,
    iid=2,
    interval=60,
    nb=nb,
    events=[dict(sensor1=random(), sensor2=random()) for i in range(nb)],
)

res = requests.request("POST", "http://localhost:8002/record", json=msg)
