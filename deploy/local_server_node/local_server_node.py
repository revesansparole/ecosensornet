"""
Very simple HTTP server in python for launching job and downloading results
"""
import json
import logging
import platform
from datetime import datetime, timedelta
from http.server import BaseHTTPRequestHandler, HTTPServer
from pathlib import Path

if platform.system() == 'Windows':
    root_fld = Path("../events")
else:
    root_fld = Path("/home/pi/events")
root_fld.mkdir(exist_ok=True)


def task_save_event(post_data):
    now = datetime.utcnow()
    data = json.loads(post_data)
    print("data", json.dumps(data, indent=2))

    nid = data['nid']
    # api = data.pop('iid')
    meas_interval = data['interval']
    dt = timedelta(seconds=meas_interval)
    # nb_meas = data.pop('nb')

    fld = root_fld / f"node_{nid}"
    fld.mkdir(exist_ok=True)

    for i, event in enumerate(data['events']):  # sorted from recent to old
        event_time = now - dt * i
        record = dict(
            nid=nid,
            date=event_time.isoformat(),
        ) | event
        print("rec", record)

        json.dump(record, open(fld / f"event_{event_time.isoformat().replace(':', '_').split('.')[0]}.json", 'w'))

    print("events")


class S(BaseHTTPRequestHandler):
    def _set_response(self, cnt_type='text/html'):
        self.send_response(200)
        self.send_header('Content-type', cnt_type)
        self.end_headers()

    def do_GET(self):
        logging.info("GET request,\nPath: %s\nHeaders:\n%s\n", str(self.path), str(self.headers))
        self._set_response()
        self.wfile.write("GET request for {}".format(self.path).encode('utf-8'))

    def do_POST(self):
        content_length = int(self.headers['Content-Length'])  # <--- Gets the size of data
        post_data = self.rfile.read(content_length)  # <--- Gets the data itself
        logging.info("POST request,\nPath: %s\nHeaders:\n%s\n\nBody:\n%s\n",
                     str(self.path), str(self.headers), post_data.decode('utf-8'))

        try:
            if self.path == "/record":
                self._set_response()
                self.wfile.write("events accepted".encode('utf-8'))
                task_save_event(post_data)
            elif self.path == "/perform":
                self._set_response()
                self.wfile.write("do smth".encode('utf-8'))
            else:
                self._set_response()
                self.wfile.write("POST request for {}".format(self.path).encode('utf-8'))
        except Exception as err:
            self._set_response()
            html = f"<html><body><h1>Error</h1>{err}</body></html>"
            self.wfile.write(html.encode('utf-8'))
            raise


def run(server_class=HTTPServer, handler_class=S, port=8002):
    logging.basicConfig(level=logging.INFO)
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    logging.info(f'Starting httpd on http://localhost:{port:d}/menu\n ...\n')
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    logging.info('Stopping httpd...\n')


if __name__ == '__main__':
    run()
