"""
Very simple HTTP server in python for launching job and downloading results
"""
import logging
import os
import platform
import subprocess
from datetime import datetime
from http.server import BaseHTTPRequestHandler, HTTPServer
from pathlib import Path
from urllib.parse import parse_qs

if platform.system() == 'Windows':
    root_fld = Path("../events")
else:
    root_fld = Path("/home/pi/events")


def task_synchronize_clocks(date_now):
    """Synchronize UTC times between client and server
    """
    print("synchro", date_now)
    if platform.system() == 'Windows':
        utc_cur = datetime.utcnow()
        return utc_cur.isoformat()

    date_arg = date_now.isoformat().split(".")[0].replace("T", " ")
    cmd = ["sudo", "timedatectl", "set-time", date_arg]
    subprocess.run(cmd)

    cmd = ["sudo", "timedatectl", "status"]
    res = subprocess.run(cmd, capture_output=True)

    return res.stdout.decode('utf-8')


def task_clock_status():
    """Check status of clock on server
    """
    if platform.system() == 'Windows':
        utc_cur = datetime.utcnow()
        return utc_cur.isoformat()

    cmd = ["sudo", "timedatectl", "status"]
    res = subprocess.run(cmd, capture_output=True)

    return res.stdout.decode('utf-8')


def task_clear_events(date_threshold):
    """Clear all events before threshold

    Args:
        date_threshold (datetime): reference date

    Returns:
        status (str): result status of operation
    """
    count_rm = 0
    for pth in root_fld.rglob("event_*.json"):
        event_date = datetime.fromisoformat(pth.name[6:-5].replace("_", ":"))
        if event_date <= date_threshold:
            pth.unlink()
            count_rm += 1

    return f"done: {count_rm:d} files have been removed"


def page_menu(fields=None):
    """main menu page
    """
    html = open("data/menu.html").read()

    if fields is None:
        nb_sensors = 0
        nb_events = 0
        for fld in root_fld.glob("*/"):
            nb_sensors += 1
            nb_events += len(tuple(fld.glob("event_*.json")))

        fields = dict(nb_events=nb_events, nb_sensors=nb_sensors)

    return html.format(preamble="", fields=fields)


def page_synchronize(fields):
    """Setup time on raspberry
    """
    preamble = ""
    if b'status' in fields:
        status = task_clock_status()
        preamble = f"<h1>Status of clock on server</h1><p>{status}</p>"
    elif b'synchronize' in fields:
        date_iso = fields[b'date_picker'][0].decode("utf-8")
        if date_iso.count(":") > 1:
            date_iso = ":".join(date_iso.split(":")[:-1])
        date_now = datetime.fromisoformat(date_iso)
        status = task_synchronize_clocks(date_now)

        preamble = f"<h1>clocks have been synchronized</h1><p>{status}</p>"

    html = open("data/synchronize.html").read()

    return html.format(preamble=preamble, fields=fields)


def page_clear(fields):
    """Clear files on raspberry
    """
    preamble = ""
    if b'date_picker' in fields:
        date_iso = fields[b'date_picker'][0].decode("utf-8")
        if date_iso.count(":") > 1:
            date_iso = ":".join(date_iso.split(":")[:-1])
        date_threshold = datetime.fromisoformat(date_iso)
        status = task_clear_events(date_threshold)

        preamble = f"<h1>Events have been removed</h1><p>{status}</p>"

    html = open("data/clear.html").read()
    return html.format(preamble=preamble, fields=fields)


class S(BaseHTTPRequestHandler):
    def _set_response(self, cnt_type='text/html'):
        self.send_response(200)
        self.send_header('Content-type', cnt_type)
        self.end_headers()

    def do_GET(self):
        logging.info("GET request,\nPath: %s\nHeaders:\n%s\n", str(self.path), str(self.headers))
        try:
            if self.path == "/menu.html":
                self._set_response()
                html = page_menu()
                self.wfile.write(html.encode('utf-8'))
            elif self.path == "/myscript.js":
                self._set_response('text/javascript')
                self.wfile.write(open("data/myscript.js", 'rb').read())
            elif self.path == "/style.css":
                self._set_response('text/css')
                self.wfile.write(open("data/style.css", 'rb').read())
            else:
                self._set_response()
                self.wfile.write("GET request for {}".format(self.path).encode('utf-8'))
        except Exception as err:
            self._set_response()
            html = f"<html><body><h1>Error</h1><p>{os.getcwd()}</p><p>{err}</p></body></html>"
            self.wfile.write(html.encode('utf-8'))

    def do_POST(self):
        content_length = int(self.headers['Content-Length'])  # <--- Gets the size of data
        post_data = self.rfile.read(content_length)  # <--- Gets the data itself
        logging.info("POST request,\nPath: %s\nHeaders:\n%s\n\nBody:\n%s\n",
                     str(self.path), str(self.headers), post_data.decode('utf-8'))
        fields = parse_qs(post_data)

        try:
            if self.path == "/menu.html":
                html = page_menu(fields)
                self._set_response()
                self.wfile.write(html.encode('utf-8'))
            elif self.path == "/synchronize.html":
                html = page_synchronize(fields)
                self._set_response()
                self.wfile.write(html.encode('utf-8'))
            elif self.path == "/clear.html":
                html = page_clear(fields)
                self._set_response()
                self.wfile.write(html.encode('utf-8'))
            else:
                self._set_response()
                self.wfile.write("POST request for {}".format(self.path).encode('utf-8'))
        except Exception as err:
            self._set_response()
            html = f"<html><body><h1>Error</h1>{err}</body></html>"
            self.wfile.write(html.encode('utf-8'))


def run(server_class=HTTPServer, handler_class=S, port=8020):
    logging.basicConfig(level=logging.INFO)
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    logging.info(f'Starting httpd on http://localhost:{port:d}/menu\n ...\n')
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    logging.info('Stopping httpd...\n')


if __name__ == '__main__':
    run()
