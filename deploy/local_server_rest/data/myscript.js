function myFunction() {
    document.getElementById("demo").innerHTML = "Paragraph changed.";
    document.getElementById("first_date").value = "2021-04-11T00:00";
}

function last_day() {
    document.getElementById("date_picker").value = new Date(Date.now() - 3.6e6 * 24).toISOString();
}
function last_week() {
    document.getElementById("date_picker").value = new Date(Date.now() - 3.6e6 * 24 * 7).toISOString();
}
function last_month() {
    document.getElementById("date_picker").value = new Date(Date.now() - 3.6e6 * 24 * 30).toISOString();
}


function set_now() {
    document.getElementById("date_picker").value = new Date(Date.now()).toISOString();
//    document.getElementById("debug").innerHTML = new Date(Date.now()).toISOString();
}
