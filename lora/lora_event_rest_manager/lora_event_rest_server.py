"""
Very simple HTTP server in python for launching job and downloading results
"""
import logging
import subprocess
import os
from datetime import datetime, timedelta
from http.server import BaseHTTPRequestHandler, HTTPServer
from pathlib import Path
from threading import Thread
from time import sleep
from urllib.parse import parse_qs
from zipfile import ZIP_DEFLATED, ZipFile

root_fld = Path("/home/pi/events")


class State:
    def __init__(self):
        self.arc_gen = None


class ArchiveGenerator(Thread):
    def __init__(self, first_date=None):
        super().__init__()
        self.pth_arc = Path("arc_events.zip")  # archive file of all events
        if self.pth_arc.exists():
            self.pth_arc.unlink()

        self._first = first_date
        self._perform = False

    def stop(self):
        self._perform = False

    def run(self):
        self._perform = True

        fhw = ZipFile(self.pth_arc, 'w', compression=ZIP_DEFLATED)
        for pth in root_fld.glob("**/*.json"):
            if not self._perform:
                fhw.close()
                return

            if self._first is None:
                arc_pth = "/".join(pth.parts[4:])
                fhw.write(pth, arc_pth)
            else:
                file_date = datetime.fromisoformat(pth.name[6:-5].replace("_", ":"))
                if file_date >= self._first:
                    arc_pth = "/".join(pth.parts[4:])
                    fhw.write(pth, arc_pth)
                else:
                    print("old", pth)

        fhw.close()


state = State()


def task_stop_arc_gen():
    if state.arc_gen is not None:
        state.arc_gen.stop()
        while state.arc_gen.is_alive():
            sleep(0.1)

        state.arc_gen = None


def task_synchronize_clocks(date_now):
    """Synchronize UTC times between client and server
    """
    cmd = ["sudo", "timedatectl", "set-ntp", "0"]
    subprocess.run(cmd)
    cmd = ["sudo", "timedatectl", "set-time", date_now.isoformat().split(".")[0].replace("T", " ")]
    subprocess.run(cmd)

    cmd = ["sudo", "timedatectl", "status"]
    res = subprocess.run(cmd, capture_output=True)

    return res.stdout.decode('utf-8')


def task_clock_status():
    """Check status of clock on server
    """
    cmd = ["sudo", "timedatectl", "status"]
    res = subprocess.run(cmd, capture_output=True)

    return res.stdout.decode('utf-8')


def task_clear_events(date_threshold):
    """Clear all events before threshold

    Args:
        date_threshold (datetime): reference date

    Returns:
        status (str): result status of operation
    """
    count_rm = 0
    for pth in root_fld.rglob("event_*.json"):
        event_date = datetime.fromisoformat(pth.name[6:-5].replace("_", ":"))
        if event_date <= date_threshold:
            pth.unlink()
            count_rm += 1

    return f"done: {count_rm:d} files have been removed"


def page_menu(fields=None):
    """main menu page
    """
    if fields is None:
        fields = {}

    preamble = ""

    html = f"""<html>
<body>
<script>{open("myscript.js").read()}</script>

{preamble}
<h1>Walou</h1>

<form method="post" action="download">
<p>Download file after: 
<input type="datetime-local" id="date_picker"
       name="date_picker" value="2021-04-11T20:44"
       min="2018-06-07T00:00" max="2030-06-01T00:00">
<input type="submit" name="download" value="Download">
<button type="button" onclick="last_day()">Last day</button>
<button type="button" onclick="last_week()">Last week</button>
<button type="button" onclick="last_month()">Last month</button>
</p>
</form>

<form method="post" action="clear">
<p><input type="submit" value="Clear">
Clear all files</p>
</form>

<form method="post" action="synchronize">
<p><input type="submit" value="Synchronize">
Synchronize clocks</p>
</form>

<h2>Debug</h2>
<p id="debug">{fields}</p>
</body>
</html>"""

    return html


def page_download(fields):
    """Launch zip and propose download link once ready
    """
    if b'download' in fields or b'date_picker' in fields:
        task_stop_arc_gen()

    if state.arc_gen is None:
        try:
            date_iso = fields[b'date_picker'][0].decode("utf-8")
            if date_iso.count(":") > 1:
                date_iso = ":".join(date_iso.split(":")[:-1])
            first_date = datetime.fromisoformat(date_iso)
        except KeyError:
            first_date = None

        state.arc_gen = ArchiveGenerator(first_date)
        state.arc_gen.start()

    if state.arc_gen.is_alive():
        html = f"""<html>
<body>
<a href='/menu'>back to menu</a>
<h1>Nothing to download</h1>
<form method="post" action="download">
<p><input type="submit" name="refresh" value="Refresh">
</p>
</form>
<h2>Debug</h2>
<p>{fields}</p>
</body>
</html>
"""
    else:
        html = f"""<html>
<body>
<a href='/menu'>back to menu</a>
<h1>File ready to download</h1>
<form method="post" action="download_zip">
<p>Download result:
<input type="submit" name="download' value="Download">
</p>
</form>
<h2>Debug</h2>
<p>{fields}</p>
</body>
</html>
"""

    return html


def page_synchronize(fields):
    """Setup time on raspberry
    """
    preamble = ""
    if b'status' in fields:
        status = task_clock_status()
        preamble = f"<h1>Status of clock on server</h1><p>{status}</p>"
    elif b'date_picker' in fields:
        date_iso = fields[b'date_picker'][0].decode("utf-8")
        if date_iso.count(":") > 1:
            date_iso = ":".join(date_iso.split(":")[:-1])
        date_now = datetime.fromisoformat(date_iso)
        status = task_synchronize_clocks(date_now)

        preamble = f"<h1>clocks have been synchronized</h1><p>{status}</p>"

    html = f"""<html>
<body>
<script>{open("myscript.js").read()}</script>
{preamble}

<a href='/menu'>back to menu</a>
<h1>Synchronize raspberry clock</h1>
<form method="post" action="synchronize">
<p>
<button type="button" onclick="set_now()">Now</button>
<input type="datetime-local" id="date_picker"
       name="date_picker" value="2021-04-11T20:44"
       min="2018-06-07T00:00" max="2030-06-01T00:00">
<input type="submit" name="synchronize" value="Synchronize">
</p>
<input type="submit" name="status" value="Status">
</form>
<h2>Debug</h2>
<p>{fields}</p>
</body>
</html>
"""

    return html


def page_clear(fields):
    """Clear files on raspberry
    """
    preamble = ""
    if b'date_picker' in fields:
        date_iso = fields[b'date_picker'][0].decode("utf-8")
        if date_iso.count(":") > 1:
            date_iso = ":".join(date_iso.split(":")[:-1])
        date_threshold = datetime.fromisoformat(date_iso)
        status = task_clear_events(date_threshold)

        preamble = f"<h1>Events have been removed</h1><p>{status}</p>"

    html = f"""<html>
<body>
<script>{open("myscript.js").read()}</script>
{preamble}

<a href='/menu'>back to menu</a>
<h1>Clear event files on raspberry</h1>
<form method="post" action="clear">
<p>Remove files before: 
<input type="datetime-local" id="date_picker"
       name="date_picker" value="2021-04-11T20:44"
       min="2018-06-07T00:00" max="2030-06-01T00:00">
<input type="submit" name="clear" value="Clear">
<button type="button" onclick="set_now()">Until now</button>
<button type="button" onclick="last_day()">Until last day</button>
<button type="button" onclick="last_week()">Until last week</button>
<button type="button" onclick="last_month()">Until last month</button>
</p>
</form>
<h2>Debug</h2>
<p>{fields}</p>
</body>
</html>
"""

    return html


class S(BaseHTTPRequestHandler):
    def _set_response(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        logging.info("GET request,\nPath: %s\nHeaders:\n%s\n", str(self.path), str(self.headers))
        self._set_response()
        try:
            if self.path == "/menu":
                html = page_menu()
                self.wfile.write(html.encode('utf-8'))
            else:
                self.wfile.write("GET request for {}".format(self.path).encode('utf-8'))
        except Exception as err:
            html = f"<html><body><h1>Error</h1><p>{os.getcwd()}</p><p>{err}</p></body></html>"
            self.wfile.write(html.encode('utf-8'))

    def do_POST(self):
        content_length = int(self.headers['Content-Length'])  # <--- Gets the size of data
        post_data = self.rfile.read(content_length)  # <--- Gets the data itself
        logging.info("POST request,\nPath: %s\nHeaders:\n%s\n\nBody:\n%s\n",
                     str(self.path), str(self.headers), post_data.decode('utf-8'))
        fields = parse_qs(post_data)

        try:
            if self.path == "/menu":
                html = page_menu(fields)
                self._set_response()
                self.wfile.write(html.encode('utf-8'))
            elif self.path == "/download":
                html = page_download(fields)
                self._set_response()
                self.wfile.write(html.encode('utf-8'))
            elif self.path == "/download_zip":
                if state.arc_gen is None or state.arc_gen.is_alive() or not state.arc_gen.pth_arc.exists():
                    raise UserWarning("Problem with generation of archive")
                else:
                    self.send_response(200)
                    self.send_header('Content-type', 'application/zip')
                    self.send_header('Content-Disposition',
                                     f'attachment; filename="{state.arc_gen.pth_arc.name}"')
                    self.end_headers()
                    self.wfile.write(open(state.arc_gen.pth_arc, 'br').read())
            elif self.path == "/synchronize":
                html = page_synchronize(fields)
                self._set_response()
                self.wfile.write(html.encode('utf-8'))
            elif self.path == "/clear":
                html = page_clear(fields)
                self._set_response()
                self.wfile.write(html.encode('utf-8'))
            else:
                self._set_response()
                self.wfile.write("POST request for {}".format(self.path).encode('utf-8'))
        except Exception as err:
            self._set_response()
            html = f"<html><body><h1>Error</h1>{err}</body></html>"
            self.wfile.write(html.encode('utf-8'))


def run(server_class=HTTPServer, handler_class=S, port=8020):
    logging.basicConfig(level=logging.INFO)
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    logging.info(f'Starting httpd on http://192.168.230.1:{port:d}/menu\n ...\n')
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    logging.info('Stopping httpd...\n')


if __name__ == '__main__':
    run()
