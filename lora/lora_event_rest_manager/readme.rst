LORA event REST
===============

Simple server to manage interaction with messages stored on gateway

ssh pi@192.168.230.1:22

How to deploy (see https://www.linode.com/docs/guides/start-service-at-boot/):
 - copy python files in/home/pi
 - copy lora_event_rest.service in /lib/systemd/system (sudo mv lora_event_rest.service /etc/systemd/system/lora_event_rest.service)
 - change permissions (sudo chmod 644 /etc/systemd/system/lora_event_rest.service)
 - start service (sudo systemctl start lora_event_rest)
 - ensure restart on startup (sudo systemctl enable lora_event_rest)
