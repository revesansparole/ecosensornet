Event logger
============

Simple server used to be called by chirpstack that save node events on disk.

ssh pi@192.168.230.1:22

How to deploy (see https://www.linode.com/docs/guides/start-service-at-boot/):
 - copy python files in/home/pi
 - copy lora_event_logger.service in /lib/systemd/system (sudo mv lora_event_logger.service /etc/systemd/system/lora_event_logger.service)
 - change permissions (sudo chmod 644 /etc/systemd/system/lora_event_logger.service)
 - start service (sudo systemctl start lora_event_logger)
 - ensure restart on startup (sudo systemctl enable lora_event_logger)
