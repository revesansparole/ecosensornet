"""
Very simple HTTP server in python to listen to POST on http://localhost:8002/events
and save them on disk
"""
from pathlib import Path
import json
import logging
from http.server import BaseHTTPRequestHandler, HTTPServer
from urllib.parse import urlparse
from datetime import datetime
from base64 import decodebytes

root_fld = Path("/home/pi/events")
root_fld.mkdir(exist_ok=True)


def task_save_event(post_data):
    data = json.loads(post_data)
    print("data", json.dumps(data, indent=2))

    dev_uid = "_".join(f"{digit:02x}" for digit in decodebytes(data["devEUI"].encode("utf-8")))
    fld = root_fld / f"dev_{dev_uid}"
    fld.mkdir(exist_ok=True)

    with open(fld / f"event_{datetime.utcnow().isoformat().replace(':', '_')}.json", 'wb') as fhw:
        fhw.write(post_data)

    print("events")


class S(BaseHTTPRequestHandler):
    def _set_response(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        logging.info("GET request,\nPath: %s\nHeaders:\n%s\n", str(self.path), str(self.headers))
        self._set_response()
        self.wfile.write("GET request for {}".format(self.path).encode('utf-8'))

    def do_POST(self):
        content_length = int(self.headers['Content-Length'])  # <--- Gets the size of data
        post_data = self.rfile.read(content_length)  # <--- Gets the data itself
        logging.info("POST request,\nPath: %s\nHeaders:\n%s\n\nBody:\n%s\n",
                     str(self.path), str(self.headers), post_data.decode('utf-8'))

        parsed = urlparse(self.path)

        if parsed.path == "/" and parsed.query == "event=up":
            self._set_response()
            self.wfile.write("events accepted".encode('utf-8'))
            task_save_event(post_data)
        else:
            self._set_response()
            self.wfile.write("POST request for {}".format(self.path).encode('utf-8'))


def run(server_class=HTTPServer, handler_class=S, port=8002):
    logging.basicConfig(level=logging.INFO)
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    logging.info(f'Starting httpd on http://localhost:{port:d}\n ...\n')
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    logging.info('Stopping httpd...\n')


if __name__ == '__main__':
    run()
