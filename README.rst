========================
ecosensornet
========================

.. {# pkglts, doc

.. image:: https://revesansparole.gitlab.io/ecosensornet/_images/badge_doc.svg
    :alt: Documentation status
    :target: https://revesansparole.gitlab.io/ecosensornet/

.. #}
.. {# pkglts, glabreport, after doc

main: |main_build|_

.. |main_build| image:: https://gitlab.com/revesansparole/ecosensornet/badges/main/pipeline.svg
.. _main_build: https://gitlab.com/revesansparole/ecosensornet/commits/main

.. #}

Network of sensors for ecology agronomy


Instructions
------------

To compile the documentation, you need a python environment with sphinx.

.. code-block:: bash

    $ conda activate myenv
    (myenv)$ cd report
    (myenv)$ make html

The resulting document should be in **report/build/html/index.html**

If you want to replay the analysis, all the scripts that generated the figures
are in the **script** folder.
