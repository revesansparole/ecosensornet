Additional boards manager URLs:
https://resource.heltec.cn/download/package_CubeCell_index.json

serial 115200


board: CubeCell-Board htcc-ab01
LORAWAN REGION: REGION_EU868
LORAWAN_CLASS: CLASS_A
LORAWAN_DEVEUI: CUSTOM
LORAWAN_NETMODE: OTAA
LORAWAN_ADR: ON
LORAWAN_UPLINKMODE: CONFIRMED
LORAWAN_Net_Reservation: OFF
LORAWAN_AT_SUPPORT: OFF
LORAWAN_RGB: ACTIVE
LoRaWan Debug Level: None
Port: COM8

If LoraWan_RGB is Active:
   RGB red means sending;
   RGB purple means joined done;
   RGB blue means RxWindow1;
   RGB yellow means RxWindow2;
   RGB green means received done;
