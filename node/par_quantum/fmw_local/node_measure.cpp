#include <ArduinoJson.h>
#include <Wire.h>

#include "component_mcp3421.h"
#include "sensor_max6675.h"

#include "node_cfg.h"

#include "node_measure.h"

#define thermoDO 13
#define thermoCS 12
#define thermoCLK 14

typedef struct {
  float par;
  float t_air;
} MeasSample;

MeasSample sample_cur;

MCP3421 light = MCP3421();
MAX6675 th = MAX6675(thermoCLK, thermoCS, thermoDO);

void setup_measure() {
  Wire.begin(); //Join I2C bus
  
  light.begin();
  if (!light.is_connected()) {
    Serial.println("Could not find a valid light sensor @" + String(light.address()) + ", check wiring!");
    pinMode(LED_BUILTIN, OUTPUT);
    for (uint8_t i = 0; i < 10; i++) {
      digitalWrite(LED_BUILTIN, HIGH);
      delay(100);
      digitalWrite(LED_BUILTIN, LOW);
      delay(200);
    }
    ESP.restart();
  }
  light.configure(MCP3421::one_shot, MCP3421::res_18, MCP3421::gain_8);

  th.begin();

}


void perform_sampling() {
  // light
  light.trigger();
  delay(10);
  while (!light.is_ready()) {
    delay(10);
  }
  sample_cur.par = light.value() * 100;  // [mV] to [µmol.m-2.s-1]

  //t_air
  sample_cur.t_air = th.read_celsius();
}

void fmt_meas_cfg(String& msg) {
  StaticJsonDocument<128> doc;

  doc["par"] = sample_cur.par;
  doc["t_air"] = sample_cur.t_air;

  serializeJsonPretty(doc, msg);
}


void fmt_meas_local(String& msg) {
  StaticJsonDocument<128> doc;

  doc["nid"] = node_id;
  doc["iid"] = 4;
  doc["interval"] = cfg.interval;
  doc["nb"] = 1;

  JsonArray events = doc.createNestedArray("events");

  JsonObject events_0 = events.createNestedObject();
  events_0["par"] = sample_cur.par;
  events_0["t_air"] = sample_cur.t_air;

  serializeJson(doc, msg);
}


void fmt_meas_remote(String& msg) {
}
