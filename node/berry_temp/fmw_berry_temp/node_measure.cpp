#include <ArduinoJson.h>
#include <Wire.h>

#include "node_cfg.h"
#include "sensor_ds18b20.h"

#include "node_measure.h"

DS18B20 ds(5);

uint8_t sid [2][8] = {  // addresses of sensors
  {40, 230, 163, 7, 214, 1, 60, 111},
  {40, 93, 56, 7, 214, 1, 60, 87},
};

typedef struct {
  float t0;
  float t1;
} MeasSample;

MeasSample sample_cur;

void setup_measure() {
  Wire.begin(); //Join I2C bus

  // Check device present
  for (uint8_t i = 0; i < 2; i++) {
    if (!ds.isConnected(sid[i])) {
      Serial.print("No device found at address ");
      for (uint8_t j = 0; j < 8; j++) {
        Serial.print(sid[i][j]);
        Serial.print(", ");
      }
      Serial.println("end");
      while (true) {
        delay(100);
      }
    }
  }
}


void perform_sampling() {
  ds.select(sid[0]);
  sample_cur.t0 = ds.getTempC();
  ds.select(sid[1]);
  sample_cur.t1 = ds.getTempC();
  yield();
}

void fmt_meas_cfg(String& msg) {
  StaticJsonDocument<128> doc;

  doc["t0"] = sample_cur.t0;
  doc["t1"] = sample_cur.t1;

  serializeJsonPretty(doc, msg);
}


void fmt_meas_local(String& msg) {
  StaticJsonDocument<256> doc;

  doc["nid"] = node_id;
  doc["iid"] = 4;
  doc["interval"] = cfg.interval;
  doc["nb"] = 1;

  JsonArray events = doc.createNestedArray("events");

  JsonObject events_0 = events.createNestedObject();
  events_0["t0"] = sample_cur.t0;
  events_0["t1"] = sample_cur.t1;
  serializeJson(doc, msg);
}


void fmt_meas_remote(String& msg) {
}
