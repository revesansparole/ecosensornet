#define DEBUG 1

#include "node_base.h"

void setup() {
#if (DEBUG == 1)
  Serial.begin(115200);
  while (!Serial) {
    delay(10);
  }
  delay(300);
#endif // DEBUG
  base_setup();
}

void loop() {
  base_loop();
}
