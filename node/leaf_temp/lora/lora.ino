#include "Arduino.h"
#include "node_basic.h"
#include "sensor_bh1750.h"
#include "sensor_mlx90614.h"
#include "sensor_ds18b20.h"


#define PAYLOAD 100

uint8_t devEui[] = { 0xff, 0x00, 0x00, 0x00, 0x00, 0x00, 0xff, 0x01 };
static uint16_t node_api = 100;

// sensors
BH1750 light;
MLX90614 th_ir;
DS18B20 ds(GPIO5);

static uint8_t meas[PAYLOAD];

uint8_t meas_nb = 0;

///////////////////////////////////////////////////
void setup() {
  boardInitMcu();
  Serial.begin(115200);
  Wire.begin(); //Join I2C bus
  pinMode(Vext, OUTPUT);  // used to power sensors

//  while (!Serial) {
//    delay(10);
//  }

  // initialize LORA
  setupLora();

  // initialize payload with node API
  meas[0] = node_api;
  meas[1] = node_api >> 8;

  // initialize sensors
  digitalWrite(Vext, LOW);
  light.begin(0x23);
  if (!light.isConnected()) {
    Serial.println("Could not find a valid light sensor, check wiring!");
    while (true);
  }
  th_ir.begin();
  if (!th_ir.isConnected()) { // Initialize thermal IR sensor
    Serial.println("Qwiic IR thermometer @" + String(th_ir.address()) + ", did not acknowledge! Freezing!");
    while (true);
  }

  if (ds.search() == 0) {
    Serial.println("Thermo sensor one wire, did not acknowledge! Freezing!");
    while (true);
  }

  digitalWrite(Vext, HIGH);
}

///////////////////////////////////////////////////
union {
  uint16_t val;
  uint8_t data[2];

} voltage;

union {
  float val;
  uint8_t data[4];
} sample;


void measure() {
  // wake up sensors
  digitalWrite(Vext, LOW);
  delay(300);

  if (meas_nb == 0) { //measure battery voltage only once
    voltage.val = getBatteryVoltage();
    Serial.println("Battery: " + String(voltage.val) + " [mV]");
    meas[2] = voltage.data[0];  // first two bytes are reserved for api
    meas[3] = voltage.data[1];
  }
  uint8_t ind = 4 + meas_nb * 16;  // current index in payload

  // light
  if (light.isConnected()) {
    sample.val = light.readLight();
  }
  else {
    Serial.println("Invalid light");
    sample.val = -999;
  }
  Serial.println("sample: " + String(ind) + " light: " + String(sample.val, 2) + " [lx]");
  for (uint8 i = 0; i < 4; i++) {
    meas[ind] = sample.data[i];
    ind ++;
  }

  // ir thermometer
  if (th_ir.read()) {
    sample.val = th_ir.ambient();
    Serial.println("sample: " + String(ind) + " ir_ambient: " + String(sample.val, 2) + " [°C]");
    for (uint8 i = 0; i < 4; i++) {
      meas[ind] = sample.data[i];
      ind ++;
    }
    sample.val = th_ir.object();
    Serial.println("sample: " + String(ind) + " ir_object: " + String(sample.val, 2) + " [°C]");
    for (uint8 i = 0; i < 4; i++) {
      meas[ind] = sample.data[i];
      ind ++;
    }
  }
  else {
    Serial.println("Invalid IR");
    sample.val = -999;
    for (uint8 i = 0; i < 4; i++) {
      meas[ind] = sample.data[i];
      ind ++;
    }
    for (uint8 i = 0; i < 4; i++) {
      meas[ind] = sample.data[i];
      ind ++;
    }
  }

  // temp sensor
  sample.val = -999;
  while (ds.selectNext()) {  // hopefuly run only once
    sample.val = ds.getTempC();
  }
  Serial.println("sample: " + String(ind) + " temp: " + String(sample.val, 2) + " [°C]");
  for (uint8 i = 0; i < 4; i++) {
    meas[ind] = sample.data[i];
    ind ++;
  }

  // disable sensors
  digitalWrite(Vext, HIGH);
}

void loop() {
  loopBeg(10000);  // one measure every 10s

  // perform measure
  measure();
  meas_nb++;

  // Send message once per minute
  if (meas_nb >= 6) {
    meas_nb = 0;
    send(meas, PAYLOAD);
  }

  loopEnd();
}
