#include <ArduinoJson.h>
#include <Wire.h>

#include "component_mcp3424.h"
#include "sensor_mlx90614.h"

#include "node_cfg.h"

#include "node_measure.h"

typedef struct {
  float ir_ambient;
  float ir_object;
  float light_same;
  float light_same_cal;
  float light_opo;
  float light_opo_cal;
} MeasSample;

MeasSample sample_cur;

MCP3424 light = MCP3424(0x68);
MLX90614 th_ir;

void setup_measure() {
  Wire.begin(); //Join I2C bus

  light.begin();
  delay(1); // MC3424 needs 300us to settle, wait 1ms
  if (!light.is_connected()) {
#if (DEBUG == 1)
    Serial.println("Could not find a valid light sensor @" + String(light.address()) + ", check wiring!");
#endif // DEBUG
    while (true);
  }

  th_ir.begin();
  if (!th_ir.isConnected()) { // Initialize thermal IR sensor
#if (DEBUG == 1)
    Serial.println("Qwiic IR thermometer @" + String(th_ir.address()) + ", did not acknowledge! Freezing!");
#endif // DEBUG
    while (true);
  }
}


void perform_sampling() {
  uint8_t err;
  long light_value = 0;
  MCP3424::Config status;

  err = light.convert_and_read(MCP3424::chn_1, MCP3424::one_shot, MCP3424::res_18, MCP3424::gain_8, 1000000, light_value, status);
  if (err) {
    sample_cur.light_opo = 999;
    sample_cur.light_opo_cal = 999;
  }
  else {
    sample_cur.light_opo = light_value;
    sample_cur.light_opo_cal = light_value;
  }
  yield();

  if (th_ir.read())  {
    sample_cur.ir_ambient = th_ir.ambient();
    sample_cur.ir_object = th_ir.object();
  }
  else {
    sample_cur.ir_ambient = 999;
    sample_cur.ir_object = 999;
  }
  yield();

  err = light.convert_and_read(MCP3424::chn_2, MCP3424::one_shot, MCP3424::res_18, MCP3424::gain_8, 1000000, light_value, status);
  if (err) {
    sample_cur.light_same = 999;
    sample_cur.light_same_cal = 999;
  }
  else {
    sample_cur.light_same = light_value;
    sample_cur.light_same_cal = light_value;
  }
  yield();
}

void fmt_meas_cfg(String& msg) {
  StaticJsonDocument<128> doc;

  doc["ir_ambient"] = sample_cur.ir_ambient;
  doc["ir_object"] = sample_cur.ir_object;
  doc["light_same"] = sample_cur.light_same;
  doc["light_same_cal"] = sample_cur.light_same_cal;
  doc["light_opo"] = sample_cur.light_opo;
  doc["light_opo_cal"] = sample_cur.light_opo_cal;

  serializeJsonPretty(doc, msg);
}


void fmt_meas_local(String& msg) {
  StaticJsonDocument<256> doc;

  doc["nid"] = node_id;
  doc["iid"] = 3;
  doc["interval"] = cfg.interval;
  doc["nb"] = 1;

  JsonArray events = doc.createNestedArray("events");

  JsonObject events_0 = events.createNestedObject();
  events_0["ir_ambient"] = sample_cur.ir_ambient;
  events_0["ir_object"] = sample_cur.ir_object;
  events_0["light_same"] = sample_cur.light_same;
  events_0["light_same_cal"] = sample_cur.light_same_cal;
  events_0["light_opo"] = sample_cur.light_opo;
  events_0["light_opo_cal"] = sample_cur.light_opo_cal;

  serializeJson(doc, msg);
}


void fmt_meas_remote(String& msg) {
}
