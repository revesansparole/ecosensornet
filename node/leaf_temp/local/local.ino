#include "node_measure.h"

unsigned long t0;

void setup() {
  Serial.begin(115200);
  while (!Serial) {
  }

  delay(1000);
  setup_measure();
}

void loop() {
  byte err;

  String meas_cur = "";
  t0 = millis();
  perform_sampling();
  fmt_meas_cfg(meas_cur);
  Serial.println("elapsed: " + String(millis() - t0));
  Serial.println(meas_cur);

  delay(1000);
}
