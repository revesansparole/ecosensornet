Quality: 0.3 mm

Walls
thickness 0.8 mm
line count 2
hor expansion 0 mm

top/bottom
thickness 0.8 mm
layers 3

infill
density 20%
pattern grid

Material
print temp 205
plate temp 60

speed 60 mm/S

cooling
fan speed 100%

support
placement everywhere
angle 45

adhesion
none

for connectors don't use support
