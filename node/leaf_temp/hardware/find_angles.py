"""
Find rotation angles for connectors
"""
from math import atan2, degrees, sqrt

b_width = 16
b_height = 18

cage_width = 60
cage_height = 50

print("link short")
dx = 0
dy = cage_width - b_height / 2
dz = cage_height
length = sqrt(dx ** 2 + dy ** 2 + dz ** 2)
print(f"length: {length:.2f}")

azim = 0
elev = atan2(dz, dy)
print(f"link azim: {degrees(azim):.2f}")
print(f"link elev: {degrees(elev):.2f}, {180 - degrees(elev):.2f}")

dx *= 10 / length
dy *= 10 / length
dz *= 10 / length
print(f"disp: {dx:.2f}, {dy:.2f}, {dz:.2f}")
ori_x = 8
ori_y = -7
ori_z = 0
print(f"end: {ori_x + dx:.2f}, {ori_y - dy:.2f}, {ori_z + dz:.2f}")
ori_x = 8
ori_y = 11
ori_z = 0
print(f"end: {ori_x + dx:.2f}, {ori_y + dy:.2f}, {ori_z + dz:.2f}")
ori_x = 0
ori_y = 0
ori_z = 0
print(f"end: {ori_x + dx * 1.12:.2f}, {ori_y + dy * 1.12:.2f}, {ori_z + dz * 1.12:.2f}")

print("link long")
dx = cage_width - b_width
dy = cage_width - b_height / 2
dz = cage_height
length = sqrt(dx ** 2 + dy ** 2 + dz ** 2)
print(f"length: {length:.2f}")

azim = atan2(dx, dy)  # rotation around Ox from Ox toward Oy
elev = atan2(dz, sqrt(dx ** 2 + dy ** 2))
print(f"link azim: {degrees(azim):.2f}")
print(f"link elev: {degrees(elev):.2f}, {90 - degrees(elev):.2f}")

dx *= 10 / length
dy *= 10 / length
dz *= 10 / length
print(f"disp: {dx:.2f}, {dy:.2f}, {dz:.2f}")
ori_x = -8
ori_y = -7
ori_z = 0
print(f"end: {ori_x - dx:.2f}, {ori_y - dy:.2f}, {ori_z + dz:.2f}")
ori_x = -8
ori_y = 11
ori_z = 0
print(f"end: {ori_x - dx:.2f}, {ori_y + dy:.2f}, {ori_z + dz:.2f}")
ori_x = 0
ori_y = 0
ori_z = 0
print(f"end: {ori_x - dx * 1.12:.2f}, {ori_y - dy * 1.12:.2f}, {ori_z + dz * 1.12:.2f}")
