#include "Arduino.h"
#include "node_basic.h"

#define PAYLOAD 50

static uint8_t meas[PAYLOAD];
uint8_t devEui[] = { 0xff, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01 };

uint16_t msg_counter = 0;

///////////////////////////////////////////////////
void setup() {
  boardInitMcu();
  Serial.begin(115200);
  while (!Serial) {
    delay(10);
  }
  setupLora();
}

///////////////////////////////////////////////////
void loop() {
  loopBeg(5000);

  // Prepare payload (dummy just for testing purpose)
  unsigned long elapsed = millis();
  Serial.printf("Elapsed=%d, hex: %X\n", elapsed, elapsed);
  uint16_t voltage = getBatteryVoltage();
  Serial.printf("Measured batt=%d, hex: %X\n", voltage, voltage);
  long sample;
  uint8 sample_addr;

  meas[0] = msg_counter;
  meas[1] = elapsed >> 24;
  meas[2] = elapsed >> 16;
  meas[3] = elapsed >> 8;
  meas[4] = elapsed;
  meas[5] = 0;
  meas[6] = voltage >> 8;
  meas[7] = voltage;
  meas[8] = 0;
  for (uint8 i = 0; i < 5; i++) {
    sample = random();
    Serial.printf("%d sample hex: %X\n", i, sample);
    sample_addr = 9 + i * 5;
    meas[sample_addr] = sample >> 24;
    meas[sample_addr + 1] = sample >> 16;
    meas[sample_addr + 2] = sample >> 8;
    meas[sample_addr + 3] = sample;
    meas[sample_addr + 4] = 0;
  }

  meas[PAYLOAD - 1] = 0xff;

  send(meas, PAYLOAD);
  msg_counter++;

  loopEnd();
}
