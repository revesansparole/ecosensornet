sensors:
  - irradiance (top, bottom for diffuse?, vis, NIR?)
  - temperature
  - humidity
  - pressure
  - CO2

harware:
  - fan to blow air on temp, rh and C02 sensor
  - battery (solar panel useless, need to asses current comsumption
  - protected from rain, humidity and direct sun radiations (except for light sensor)
  