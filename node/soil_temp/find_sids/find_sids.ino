#include "sensor_ds18b20.h"

DS18B20 ds(GPIO5);
//DS18B20 ds(D1);

void setup() {
  boardInitMcu();
  Serial.begin(115200);
  while (!Serial) {
  }
  delay(1000);

  Serial.println("setup!");
//  if (ds.search() == 0) {
//    Serial.println("Thermo sensor one wire, did not acknowledge! Freezing!");
//    while (true);
//  }


}

void loop() {
  Serial.println("try reading");
  while (ds.selectNext()) {
    switch (ds.getFamilyCode()) {
      case MODEL_DS18S20:
        Serial.println("Model: DS18S20/DS1820");
        break;
      case MODEL_DS1822:
        Serial.println("Model: DS1822");
        break;
      case MODEL_DS18B20:
        Serial.println("Model: DS18B20");
        break;
      default:
        Serial.println("Unrecognized Device");
        break;
    }

    uint8_t address[8];
    ds.getAddress(address);

    Serial.print("Address:");
    for (uint8_t i = 0; i < 8; i++) {
      Serial.print(" ");
      Serial.print(address[i]);
    }
    Serial.println();

    Serial.print("Resolution: ");
    Serial.println(ds.getResolution());

    Serial.print("Power Mode: ");
    if (ds.getPowerMode()) {
      Serial.println("External");
    } else {
      Serial.println("Parasite");
    }

    Serial.print("Temperature: ");
    Serial.print(ds.getTempC());
    Serial.print(" C / ");
    Serial.print(ds.getTempF());
    Serial.println(" F");
    Serial.println();
  }

  delay(1000);

}
