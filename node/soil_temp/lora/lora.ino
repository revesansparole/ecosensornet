#include "Arduino.h"
#include "node_basic.h"
#include "sensor_bh1750.h"
#include "sensor_mlx90614.h"
#include "sensor_ds18b20.h"


#define PAYLOAD 100

uint8_t devEui[] = { 0xff, 0x00, 0x00, 0x00, 0x00, 0x00, 0xfe, 0x01 };
//uint8_t devEui[] = { 0xff, 0x00, 0x00, 0x00, 0x00, 0x00, 0xfe, 0x02 };
static uint16_t node_api = 101;

// sensors
BH1750 light1;
BH1750 light2;
DS18B20 ds(GPIO5);

uint8_t temp1[] = { 40, 37, 63, 7, 214, 1, 60, 63 };
//uint8_t temp1[] = { 40, 114, 157, 7, 214, 1, 60, 36 };
uint8_t temp2[] = { 40, 95, 251, 7, 214, 1, 60, 85 };
//uint8_t temp2[] = { 40, 167, 18, 7, 214, 1, 60, 175 };

static uint8_t meas[PAYLOAD];

uint8_t meas_nb = 0;

///////////////////////////////////////////////////
void setup() {
  boardInitMcu();
  Serial.begin(115200);
  Wire.begin(); //Join I2C bus
  pinMode(Vext, OUTPUT);  // used to power sensors

  // initialize LORA
  setupLora();

  // initialize payload with node API
  meas[0] = node_api;
  meas[1] = node_api >> 8;

  // initialize sensors
  digitalWrite(Vext, LOW);
  light1.begin(0x23);
  if (!light1.isConnected()) {
    Serial.println("Could not find a valid light1 sensor @" + String(light1.address()) + ", check wiring!");
    while (true);
  }
  light2.begin(0x5c);
  if (!light2.isConnected()) {
    Serial.println("Could not find a valid light2 sensor @" + String(light2.address()) + ", check wiring!");
    while (true);
  }

  if (ds.search() == 0) {
    Serial.println("Thermo sensor one wire, did not acknowledge! Freezing!");
    while (true);
  }

  // digitalWrite(Vext, HIGH);
}

///////////////////////////////////////////////////
union {
  uint16_t val;
  uint8_t data[2];

} voltage;

union {
  float val;
  uint8_t data[4];
} sample;


void measure() {
  // wake up sensors
  //  digitalWrite(Vext, LOW);
  //  delay(300);

  if (meas_nb == 0) { //measure battery voltage only once
    voltage.val = getBatteryVoltage();
    Serial.println("Battery: " + String(voltage.val) + " [mV]");
    meas[2] = voltage.data[0];  // first two bytes are reserved for api
    meas[3] = voltage.data[1];
  }
  uint8_t ind = 4 + meas_nb * 16;  // current index in payload

  // light1
  if (light1.isConnected()) {
    sample.val = light1.readLight();
  }
  else {
    Serial.println("Invalid light1");
    sample.val = -999;
  }
  Serial.println("sample: " + String(ind) + " light1: " + String(sample.val, 2) + " [lx]");
  for (uint8 i = 0; i < 4; i++) {
    meas[ind] = sample.data[i];
    ind ++;
  }

  // light2
  if (light2.isConnected()) {
    sample.val = light2.readLight();
  }
  else {
    Serial.println("Invalid light2");
    sample.val = -999;
  }
  Serial.println("sample: " + String(ind) + " light2: " + String(sample.val, 2) + " [lx]");
  for (uint8 i = 0; i < 4; i++) {
    meas[ind] = sample.data[i];
    ind ++;
  }

  // temp1
  ds.select(temp1);
  sample.val = ds.getTempC();
  Serial.println("sample: " + String(ind) + " temp1: " + String(sample.val, 2) + " [°C]");
  for (uint8 i = 0; i < 4; i++) {
    meas[ind] = sample.data[i];
    ind ++;
  }

  // temp2
  ds.select(temp2);
  sample.val = ds.getTempC();
  Serial.println("sample: " + String(ind) + " temp2: " + String(sample.val, 2) + " [°C]");
  for (uint8 i = 0; i < 4; i++) {
    meas[ind] = sample.data[i];
    ind ++;
  }

  // disable sensors
  // digitalWrite(Vext, HIGH);
}

void loop() {
  loopBeg(10000);  // one measure every 10s

  // perform measure
  measure();
  meas_nb++;

  // Send message once per minute
  if (meas_nb >= 6) {
    meas_nb = 0;
    send(meas, PAYLOAD);
  }

  loopEnd();
}
