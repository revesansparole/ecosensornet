#include "node_basic.h"
#include "sensor_ds18b20.h"

DS18B20 ds(GPIO5);

uint8_t temp [5][8] = {  // addresses of sensors (top first)
  {40, 5, 39, 149, 240, 1, 60, 104},
  {40, 195, 170, 149, 240, 1, 60, 98},
  {40, 27, 204, 149, 240, 1, 60, 138},
  {40, 40, 79, 149, 240, 1, 60, 187},
  {40, 98, 118, 149, 240, 1, 60, 118}
};

void setup() {
  boardInitMcu();

  //Vext ON
  pinMode(Vext, OUTPUT);
  digitalWrite(Vext, LOW);

  Serial.begin(115200);
  while (!Serial) {
  }

  delay(1000);
  Serial.println("Devices: " + String(ds.search()) + " [#]");
}

void loop() {
  loopBeg(1000);  // one measure every 1s
  for (uint i = 0; i < 5; i++) {
    ds.select(temp[i]);
    Serial.println("Temp" + String(i) + ": " + String(ds.getTempC(), 2) + " [°C]");
  }
  Serial.println();
  loopEnd();

}
