#include "Arduino.h"

#include "../config/node_cfg.h"

#include "mode_lora.h"


void mode_lora::setup() {
#if (DEBUG == 1)
  Serial.println("mode LORA");
  Serial.println("deepsleep");
  Serial.flush();
#endif // DEBUG
  ESP.deepSleep(cfg.interval * 1e6);  // TODO rm time since reboot
}


void mode_lora::loop() {
#if (DEBUG == 1)
  Serial.println("UserWarning, not supposed to loop in remote");
#endif // DEBUG
}