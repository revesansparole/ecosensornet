#ifndef MODE_WIFI_H
#define MODE_WIFI_H

#include "../settings.h"

namespace mode_wifi {

void connect_wifi(byte);

void setup();
void loop();

String fmt_url_record();

}  // namespace mode_wifi

#endif //  MODE_WIFI_H
