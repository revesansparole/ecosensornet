#include <ArduinoJson.h>
#include <LittleFS.h>

#include "calibration.h"

Calibration calib;

void calibration::setup() {
  calibration::load_file();
}


void calibration::load_file() {
  StaticJsonDocument<384> doc;
  File fhr = LittleFS.open("calib.json", "r");
  deserializeJson(doc, fhr);
  fhr.close();

#if (DEBUG == 1)
  Serial.println("Loaded calib:");
  String msg = "";
  serializeJsonPretty(doc, msg);
  Serial.println(msg);
#endif

  for (uint8_t i = 0; i < 3; i++) {
    calib.dendro[i] = doc["dendro"]["coeffs"][i];
  }

  yield();
}


void calibration::save_file() {
  StaticJsonDocument<384> doc;

  JsonArray dendro_coeffs = doc["dendro"].createNestedArray("coeffs");
  for (uint8_t i = 0; i < 3; i++) {
    dendro_coeffs.add(calib.dendro[i]);
  }

  File fhw = LittleFS.open("calib.json", "w");
  serializeJsonPretty(doc, fhw);
  fhw.close();

#if (DEBUG == 1)
  Serial.println("Saved calib:");
  String msg = "";
  serializeJsonPretty(doc, msg);
  Serial.println(msg);
#endif

  yield();
}
