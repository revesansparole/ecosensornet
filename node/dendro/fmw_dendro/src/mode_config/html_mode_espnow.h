#ifndef MODE_ESPNOW_HTML_H
#define MODE_ESPNOW_HTML_H

#include "../settings.h"

namespace mode_espnow {

String processor(const String& key);
void handle_html();
void handle_chg_mode();

}  // namespace mode_espnow

#endif //  MODE_ESPNOW_HTML_H
