#include "../calibration/calibration.h"
#include "../config/node_cfg.h"
#include "../mode_config/mode_config.h"

#include "html_calibration.h"

String sci(float val) {
    char buffer[16];
    sprintf(buffer,"%.4e", val);
    return String(buffer);
}

String calibration::processor(const String& key) {
  if (key == "val_cur") {
    return meas_cur;
  }
  else if (key == "nid") {
    return node_id;
  }
  else if (key == "dendro_coeff0") {
    return sci(calib.dendro[0]);
  }
  else if (key == "dendro_coeff1") {
    return sci(calib.dendro[1]);
  }
  else if (key == "dendro_coeff2") {
    return sci(calib.dendro[2]);
  }

  return "Key not found";
}


void calibration::handle_html() {
#if (DEBUG == 1)
  Serial.println("serving sampling");
#endif
  if (server.method() == HTTP_POST) {
#if (DEBUG == 1)
    Serial.println("POST to calibration.html");
    String message = "POST form was:\n";
    for (uint8_t i = 0; i < server.args(); i++) {
      message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
    }
    Serial.println(message);
#endif

    for (uint8_t i = 0; i < server.args(); i++) {
      if (server.argName(i) == "dendro_coeff0") {
        calib.dendro[0] = server.arg(i).toFloat();
      }
      else if (server.argName(i) == "dendro_coeff1") {
        calib.dendro[1] = server.arg(i).toFloat();
      }
      else if (server.argName(i) == "dendro_coeff2") {
        calib.dendro[2] = server.arg(i).toFloat();
      }
    }
    calibration::save_file();
  }

  server.process_and_send("/calibration.html", calibration::processor);
}

