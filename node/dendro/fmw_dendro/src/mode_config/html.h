#ifndef MODE_CONFIG_HTML_H
#define MODE_CONFIG_HTML_H

#include "../settings.h"

namespace mode_config {

String processor(const String& key);
void handle_html();

void handle_favicon();
void handle_style();
void handle_not_found();
void handle_save_cfg_file();

}  // namespace mode_config

#endif //  MODE_CONFIG_HTML_H
