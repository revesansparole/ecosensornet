#include <ArduinoJson.h>
#include <component_ltc2451.h>
#include <Wire.h>

#include "../calibration/calibration.h"

#include "node_measure.h"

LTC2451 adc = LTC2451();

measure::Sample sample_cur;

void measure::setup() {
  Wire.begin(); //Join I2C bus

  adc.begin();

  // Check device present
  if (!adc.is_connected()) {
#if (DEBUG == 1)
    Serial.print("No device found at address ");
    Serial.println(adc.address(), HEX);
#endif // DEBUG
    pinMode(LED_BUILTIN, OUTPUT);
    for (uint8_t i = 0; i < 10; i++) {
      digitalWrite(LED_BUILTIN, HIGH);
      delay(100);
      digitalWrite(LED_BUILTIN, LOW);
      delay(200);
    }
    ESP.restart();
  }
}


void measure::sample(){
  long value;
  adc.convert();
  while (!adc.sample_ready()) {
    delay(100);
  }

  if (adc.read(value)) {
#if (DEBUG == 1)
    Serial.println("Convert error");
#endif // DEBUG
  }

  sample_cur.dendro = value;
  sample_cur.dendro_cal = calib.dendro[0] + calib.dendro[1]  * value + calib.dendro[2]  * value * value;
  yield();
}


void measure::fmt_config(String& msg) {
  StaticJsonDocument<128> doc;

  doc["dendro"] = sample_cur.dendro;
  doc["dendro_cal"] = sample_cur.dendro_cal;

  serializeJsonPretty(doc, msg);
}
