#ifndef CALIBRATION_H
#define CALIBRATION_H

#include "../settings.h"

typedef struct {
  float dendro[3];
} Calibration;

extern Calibration calib;

namespace calibration {

void setup();
void load_file();
void save_file();

}  // namespace calibration

#endif //  CALIBRATION_H
