#include <LittleFS.h>

#include "src/calibration/calibration.h"
#include "src/measure/node_measure.h"

unsigned long t0;

void setup() {
  Serial.begin(115200);
  while (!Serial) {
    delay(10);
  }
  delay(300);

  LittleFS.begin();
  calibration::setup();
  measure::setup();
}

void loop() {
  String msg = "";

  t0 = millis();
  measure::sample();
  measure::fmt_config(msg);

  Serial.println("elapsed: " + String(millis() - t0));
  Serial.println(msg);

  delay(1000);
}
