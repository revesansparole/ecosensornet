#include <Wire.h>

#include <MCP3421.h>

CMCP3421 coMCP3421(15.625e-3);

void setup() {
  Serial.begin(115200);
  pinMode(Vext, OUTPUT);

  Wire.begin(); //Joing I2C bus

  coMCP3421.Init();
  Serial.println("Init");

}

void loop() {
  float   fValue;

  //Vext ON
  digitalWrite(Vext, LOW);
  delay(1000);

  coMCP3421.Trigger();
  delay(1000);
  if (coMCP3421.IsReady())
  {
    fValue   = coMCP3421.ReadValue();
    Serial.println("Voltage = " + String(fValue, 2) + " [mV]");
  }
  else
  {
    Serial.printf(".");
  }
  
  digitalWrite(Vext, HIGH);
  delay(1000);
}
