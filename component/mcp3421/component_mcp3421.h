#ifndef COMPONENT_MCP3421_H
#define COMPONENT_MCP3421_H

#include <Arduino.h>
#include <Wire.h>

class MCP3421 {
  public:
    static const uint8_t  DefaultAddress = 0x68;

    enum Mode {
      one_shot = 0,
      continuous,
    };

    enum Gain {
      gain_1 = 0,
      gain_2,
      gain_4,
      gain_8,
    };

    enum Resolution {
      res_12 = 0,
      res_14,
      res_16,
      res_18,
    };

  public:
    MCP3421(const uint8_t address  = DefaultAddress);
    void begin(TwoWire& wirePort = Wire);
    bool is_connected();
    uint8_t address(void) const {
      return _address;
    }
    void configure(const Mode mode = one_shot,
                   const Resolution resolution = res_12,
                   const Gain gain = gain_1);
    bool is_ready();
    void trigger();
    inline int32_t raw_value() {
      return _raw_value;
    }
    float value();

  private:
    typedef union {
      struct {
        uint8_t GAIN : 2;
        uint8_t RES  : 2;
        uint8_t OC   : 1;
        uint8_t Cx   : 2;
        uint8_t RDY  : 1;
      } bit;
      uint8_t reg;
    } Config;

    uint8_t _address;
    TwoWire* _wire;

    int32_t _raw_value;
    Config _cfg;
    Config _status;

    int _read_i2c();
    void _write_i2c(uint8_t data);
};

#endif // COMPONENT_MCP3421_H
