#include <Arduino.h>

#include "component_mcp3424.h"

// Assuming a 100kHz clock the address and config byte take 18 clock
// cycles, or 180 microseconds. Use a timeout of 250us to be safe.

const MCP3424::Channel MCP3424::chn_1 = Channel(0x00);
const MCP3424::Channel MCP3424::chn_2 = Channel(0x20);
const MCP3424::Channel MCP3424::chn_3 = Channel(0x40);
const MCP3424::Channel MCP3424::chn_4 = Channel(0x60);

const MCP3424::Mode MCP3424::one_shot = Mode(0x00);
const MCP3424::Mode MCP3424::continuous = Mode(0x10);

const MCP3424::Resolution MCP3424::res_12 = Resolution(0x00);
const MCP3424::Resolution MCP3424::res_14 = Resolution(0x04);
const MCP3424::Resolution MCP3424::res_16 = Resolution(0x08);
const MCP3424::Resolution MCP3424::res_18 = Resolution(0x0c);

const MCP3424::Gain MCP3424::gain_1 = Gain(0x00);
const MCP3424::Gain MCP3424::gain_2 = Gain(0x01);
const MCP3424::Gain MCP3424::gain_4 = Gain(0x02);
const MCP3424::Gain MCP3424::gain_8 = Gain(0x03);


void MCP3424::normalise(long &result, Config config) {
  /* Resolution is 12, 14, 16,or 18; gain is 1, 2, 4, or 8. Shift
     least places necessary such that all possibilities can be
     accounted for:

     18 - resolution + 3 - log2(gain)

     Largest shift is for resolution==12 and gain==1 (9 places)
     Smallest is for resolution==18 and gain==8 (0 places) This means
     that the lowest 21 bits of the long result are used and that up
     to 1024 results can be safely accumulated without
     underflow/overflow.
  */
  result <<= (21 - int(config.get_resolution()) - config.get_gain().log2());
}


MCP3424::MCP3424(void) : _address(0x68) {}

MCP3424::MCP3424(uint8_t address) : _address(address) {}


bool MCP3424::begin(TwoWire& wirePort) {
  _wire = &wirePort;

  _wire->beginTransmission(0x00);
  _wire->write(0x06);
  return _wire->endTransmission();
}


bool MCP3424::is_connected() {
  _wire->requestFrom(_address, (uint8_t)1);
  return _wire->available();
}


MCP3424::error_t MCP3424::configure(const Config &config) const {
  _wire->beginTransmission(_address);
  _wire->write(config.val);
  if (_wire->endTransmission()) {
    return errorConfigureFailed;
  }
  else {
    return errorNone;
  }
}


MCP3424::error_t MCP3424::convert(Channel channel, Mode mode, Resolution resolution, Gain gain) {
  return convert(Config(channel, mode, resolution, gain));
}


MCP3424::error_t MCP3424::convert(const Config &config) const {
  _wire->beginTransmission(_address);
  _wire->write(config.val | newConversionMask);
  if (_wire->endTransmission()) {
    return errorConvertFailed;
  }
  else {
    return errorNone;
  }
}


MCP3424::error_t MCP3424::read(long &result, Config& status) const {
  // Read 4 bytes, the 4th byte will configuration. From that deduce
  // if 18 bit conversion. If not use the 3rd byte, as that is the
  // most appropriate configuration value (ready may have changed).
  const uint8_t len = 4;
  uint8_t buffer[len] = {};
  _wire->requestFrom(_address, len);
  if (_wire->available() != len) {
    return errorReadFailed;
  }

  for (uint8_t i = 0; i < len; ++i) {
    buffer[i] = _wire->read();
  }

  uint8_t dataBytes;
  if ((buffer[3] & 0x0c) == 0x0c) {
    status = Config(buffer[3]); // 18 bit conversion
    dataBytes = 3;
  }
  else {
    status = Config(buffer[2]);
    dataBytes = 2;
  }

  if ((status & notReadyMask) != 0) {
    return errorConversionNotReady;
  }

  long signBit = 0;    // Location of sign bit
  long signExtend = 0; // Bits to be set if sign is set
  switch (int(status.get_resolution())) {
    case 12:
      signBit = 0x800;
      signExtend = 0xFFFFF000;
      break;
    case 14:
      signBit = 0x2000;
      signExtend = 0xFFFFC000;
      break;
    case 16:
      signBit = 0x8000;
      signExtend = 0xFFFF0000;
      break;
    case 18:
      signBit = 0x20000;
      signExtend = 0xFFFC0000;
      break;
  }

  result = 0;
  for (uint8_t i = 0; i < dataBytes; ++i) {
    result <<= 8;
    result |= (long)buffer[i];
  }

  // Fill/blank remaining bits
  if ((result & signBit) != 0)
    result |= signExtend; // Sign bit is set, sign-extend

  return errorNone;
}



MCP3424::error_t MCP3424::convert_and_read(Channel channel, Mode mode, Resolution resolution, Gain gain, unsigned long timeout, long &result, Config &status) {
  error_t err = convert(channel, mode, resolution, gain);
  if (err != errorNone) {
    return err;
  }

  unsigned long t = micros() + timeout;
  unsigned long convTime = resolution.get_conversion_time();
  if (convTime > 16383) {
    // Unreliable (see arduino reference), use delay() instead
    convTime /= 1000;
    delay(convTime);
  }
  else {
    delayMicroseconds(convTime);
  }

  do {
    err = read(result, status);
    if (!err && status.is_ready())
      return err;
  } while (long(micros() - t) < 0);

  return errorReadTimeout;
}


unsigned long MCP3424::Resolution::get_conversion_time(void) const {
  switch ((int)(*this)) {
    case 12:
      return 4167; // 240 SPS
    case 14:
      return 16667; // 60 SPS
    case 16:
      return 66667; // 15 SPS
    case 18:
      return 266667; // 3.75 SPS
  }
  return 0; // Shouldn't happen
}


unsigned long MCP3424::Config::get_conversion_time(void) const {
  return Resolution(val).get_conversion_time();
}
