#ifndef COMPONENT_LTC2451_H
#define COMPONENT_LTC2451_H

#include <Wire.h>

class LTC2451 {
  public:

    LTC2451(void);
    LTC2451(uint8_t address);

    bool begin(TwoWire &wirePort = Wire);
    bool is_connected() const;

    /** Return the I2C address used for communicating with this device.
    */
    uint8_t address(void) const {
      return _address;
    }

    /** Launch new conversion.
    */
    void convert();
    
    /** Test whether conversion has finished.
    */
    bool sample_ready();

    /** Read the sample value from the device.
    */
    bool read(long &result) const;

  private:
    uint8_t _address;
    TwoWire* _wire;
    long _last_meas;
};

#endif  // COMPONENT_LTC2451_H
