#include <Arduino.h>

#include "component_ltc2451.h"


LTC2451::LTC2451(void) : _address(0x14) {}

LTC2451::LTC2451(uint8_t address) : _address(address) {}


bool LTC2451::begin(TwoWire& wirePort) {
  _wire = &wirePort;

  _wire->beginTransmission(0x00);
  _wire->write(0x06);
  return _wire->endTransmission();
}


bool LTC2451::is_connected() const {
  _wire->requestFrom(_address, (uint8_t)1);
  return _wire->available();
}

void LTC2451::convert() {
  _wire->requestFrom(_address, (uint8_t)2);  // perform read to launch new conversion
  _last_meas = -1;
}

bool LTC2451::sample_ready() {
  if (_wire->requestFrom(_address, (uint8_t)2) != (uint8_t)2) {
    return false;
  }

  _last_meas = _wire->read() << 8 | _wire->read();
  return true;
}

bool LTC2451::read(long &result) const {
  if (_last_meas < 0) {
    return true;
  }
  result = _last_meas;
  return false;
}
