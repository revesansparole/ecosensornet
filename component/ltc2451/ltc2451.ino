#include <Wire.h>

#include "component_ltc2451.h"

LTC2451 adc = LTC2451();

void setup(void) {
  Serial.begin(115200);
  Wire.begin();
  delay(300);

  adc.begin();

  // Check device present
  if (!adc.is_connected()) {
    Serial.print("No device found at address ");
    Serial.println(adc.address(), HEX);
    while (1)
      ;
  }

}

void loop(void) {
  Serial.println("Loop");
  
  long value = 0;
  adc.convert();
  while(!adc.sample_ready()) {
    Serial.println("Waiting");
    delay(100);
  }

  bool err = adc.read(value);
  if (err) {
    Serial.println("Convert error");
  }
  else {
    Serial.println("ADC: " + String(value));
  }

  delay(5000);
}
