import json
import sys
from datetime import datetime, timedelta
from pathlib import Path

import serial

try:
    port = sys.argv[1]
except IndexError:
    port = 'COM13'

root_fld = Path('events')
root_fld.mkdir(exist_ok=True)


def task_save_event(post_data):
    now = datetime.utcnow()
    data = json.loads(post_data)

    nid = data['nid']
    meas_interval = data['interval']
    dt = timedelta(seconds=meas_interval)

    fld = root_fld / f"node_{nid}"
    fld.mkdir(exist_ok=True)

    for i, event in enumerate(data['events']):  # sorted from recent to old
        event_time = now - dt * i
        record = dict(
            nid=nid,
            date=event_time.isoformat(),
        ) | event
        print("rec", record)

        json.dump(record, open(fld / f"event_{event_time.isoformat().replace(':', '_').split('.')[0]}.json", 'w'))


ser = serial.Serial()
ser.port = port
ser.baudrate = 115200
ser.timeout = 1  # [s]
# ser.rts = False
# ser.dtr = False
ser.open()  # need to do it exactly like that to ensure no reset signal is send

while True:
    line = ser.readline()
    if line:
        if line.startswith(b"JSON:"):
            task_save_event(line[6:])
        else:
            print("msg:", line)
