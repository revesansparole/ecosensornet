#include <esp_now.h>
#include <WiFi.h>

uint32_t ellapsed;
byte maca[6];

// Callback function that will be executed when data is received
void OnDataRecv(const uint8_t* mac, const uint8_t* data, int len) {
  Serial.print("Bytes received: ");
  Serial.println(len);
  Serial.print("JSON: ");
  String msg = (char*) data;
  Serial.println(msg);
}

void setup() {
  // Initialize Serial Monitor
  Serial.begin(115200);

  // Set device as a Wi-Fi Station
  WiFi.mode(WIFI_STA);
  delay(2000);

  // Init ESP-NOW
  if (esp_now_init() != ESP_OK) {
    Serial.println("Error initializing ESP-NOW");
    return;
  } else {
    WiFi.macAddress(maca);

    char buffer[18];
    sprintf(buffer,
            "MAC: %02x%02x%02x%02x%02x%02x",
            maca[0], maca[1], maca[2], maca[3], maca[4], maca[5]
           );
    Serial.println(buffer);
  }

  // Once ESPNow is successfully Init, we will register for recv CB to
  // get recv packer info
  esp_now_register_recv_cb(OnDataRecv);

  ellapsed = millis();
}

void loop() {
  if (millis() - ellapsed > 10000) {
    ellapsed = millis();
    char buffer[18];
    sprintf(buffer,
            "MAC: %02x%02x%02x%02x%02x%02x",
            maca[0], maca[1], maca[2], maca[3], maca[4], maca[5]
           );
    Serial.println(buffer);
  }
}
